<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="frontend/local/app.css">
    <title>Studying center</title>
</head>
<body class="body">
<header class="header">
    <div class="container container_narrow">
        <div class="header__item">
            <a href="#" class="header__brand header__logo" title="Главная"></a>
        </div>
        <div class="header__item">
            <div class="header__slogan">Простые решения<br>для вашего бизнеса</div>
        </div>
        <div class="header__item">
            <div class="header__location-and-tel">
                <div class="header__location">
                    <button class="header-location__btn header__title"><span class="header-location__showcase"><span class="header-location__link icon icon_arrow-angle-down">Пятигорск</span></span></button>
                    <div class="header-location__list">
                        <div class="header-location__item header-location__item_active">Пятигорск</div>
                        <div class="header-location__item">Ставрополь</div>
                        <div class="header-location__item">Черкесск</div>
                    </div>
                </div>
                <div class="header__text header__tel">+7 (8793) 97-34-34 </div>
            </div>
        </div>
        <div class="header__item">
            <div class="header__mail">
                <div class="header__title">Эл. почта</div>
                <div class="header__text">info@mskmv.ru</div>
            </div>
        </div>
        <div class="header__item">
            <a href="#" class="header__brand header__1c" title="1C:Центр Сопровождения"></a>
        </div>
    </div>
</header>
<nav class="nav">
    <div class="container container_narrow">
        <ul class="nav__list clearfix">
            <li class="nav__item">
                <a href="#" class="link nav__link" title="О нас"><span class="nav__undescore">О нас</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Продукты"><span class="nav__undescore">Продукты</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Услуги"><span class="nav__undescore">Услуги</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Сопровождение"><span class="nav__undescore">1С:Сопровождение</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Учебный центр"><span class="nav__undescore">1С:Учебный центр</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Отчетность"><span class="nav__undescore">1С:Отчетность</span></a>
            </li>
        </ul>
    </div>
</nav>
<div class="breadcrumbs">
    <div class="container container_narrow">
        <a href="#" class="link breadcrumbs__link icon icon_arrow-right" title="Главная">Главная</a>
        <a href="#" class="link breadcrumbs__link icon icon_arrow-right" title="Наши услуги">Наши услуги</a>
        <a href="#" class="link breadcrumbs__link" title="1C : Учебный центр">1C : Учебный центр</a>
    </div>
</div>
<div class="main clearfix">
    <div class="container container_narrow">
        <div class="sidebar">
            <div class="sidebar__menu">
                <a href="#" class="link sidebar-menu__link sidebar-menu__link_active" title="1С : Учебный центр">1С : Учебный центр</a>
                <a href="#" class="link sidebar-menu__link" title="Электронная отчетность из 1С : Предприятия 8">Электронная отчетность<br>из 1С : Предприятия 8</a>
                <a href="#" class="link sidebar-menu__link" title="Центр сопровождение 1С, ИТС Информационно технологическое сопровождение">Центр сопровождение 1С, ИТС Информационно технологическое сопровождение</a>
                <a href="#" class="link sidebar-menu__link" title="Настройка и внедрение">Настройка и внедрение</a>
                <a href="#" class="link sidebar-menu__link" title="Консультации по выбору программного обеспечения">Консультации по выбору программного обеспечения</a>
                <a href="#" class="link sidebar-menu__link" title="Доставка и установка">Доставка и установка</a>
            </div>
            <div class="sidebar__banner sidebar__banner_v1"></div>
            <div class="sidebar__banner sidebar__banner_v2"></div>
        </div>
        <h1 class="h1">Учебный центр</h1>
        <div class="content content_w-sidebar">
            <div class="studying-center text">
                <p>Основная цель Учебного Центра – предоставление пользователям программ «1С» возможности повысить уровень знаний и эффективность своей работы. Основываясь на своем значительном опыте работы, мы можем гарантировать Вам самый высокий уровень обучения. Наш центр предлагает курсы и семинары, ориентированные на любой уровень пользователей программ «1С».</p>
                <h2 class="studying-center__subheader">Мы предлагаем</h2>
                <ul class="text__list studying-center__list">
                    <li class="text-list__item studying-center__item">
                        <span class="text-list__marker icon icon_arrow-right"></span>
                        <a href="#" title="Курсы, проводимые по сертифицированной фирмой «1С» методике" class="link text-list__title studying-center__title">Курсы, проводимые по сертифицированной фирмой «1С» методике</a>
                        <div class="text-list__desc studying-center__desc">Курсы предназначены для пользователей программ «1С: Предприятие 8»</div>
                    </li>
                    <li class="text-list__item studying-center__item">
                        <span class="text-list__marker icon icon_arrow-right"></span>
                        <a href="#" title="Сертифицированные курсы фирмы «1С» для школьников" class="link text-list__title studying-center__title">Сертифицированные курсы фирмы «1С» для школьников </a>
                        <div class="text-list__desc studying-center__desc">Курсы предназначены для школьников в возрасте 13-17 лет</div>
                    </li>
                    <li class="text-list__item studying-center__item">
                        <span class="text-list__marker icon icon_arrow-right"></span>
                        <a href="#" title="Консультационные семинары" class="link text-list__title studying-center__title">Консультационные семинары</a>
                        <div class="text-list__desc studying-center__desc">Для специалистов желающих углубленно освоить все аспекты автоматизированного учетас применением  продуктов фирмы «1С»</div>
                    </li>
                    <li class="text-list__item studying-center__item">
                        <span class="text-list__marker icon icon_arrow-right"></span>
                        <a href="#" title="Эксклюзивные мастер-классы" class="link text-list__title studying-center__title">Эксклюзивные мастер-классы</a>
                        <div class="text-list__desc studying-center__desc">Уникальные тренинги, как дополнение к имеющейся номенклатуре профессиональных учебных курсов и консультационных семинаров</div>
                    </li>
                    <li class="text-list__item studying-center__item">
                        <span class="text-list__marker icon icon_arrow-right"></span>
                        <a href="#" title="Авторские курсы" class="link text-list__title studying-center__title">Авторские курсы</a>
                    </li>
                </ul>
                <div class="studying-center__btn-block">
                    <div class="btn btn_linear studying-center__btn">
                        <a href="#" class="link btn__link studying-center__btn-link" title="Задать вопрос">Задать вопрос</a>
                    </div>
                    <div class="btn btn_main studying-center__btn">
                        <a href="#" class="link btn__link studying-center__btn-link" title="Подобрать курс">Подобрать курс</a>
                    </div>
                </div>
                <h2 class="studying-center__subheader">Формы обучения</h2>
                <table class="text__table studying-center__table">
                    <tbody>
                    <tr>
                        <th>Групповая</th>
                        <th>Корпоративная</th>
                        <th>Индивидуальная</th>
                    </tr>
                    <tr>
                        <td>Группы от 6 до 10 человек</td>
                        <td>Группы от 5 человек</td>
                        <td>Персональная программа обучения</td>
                    </tr>
                    <tr>
                        <td>Практические занятия на основе теории</td>
                        <td>Выбор места проведения занятий</td>
                        <td>Выбор места проведения занятий</td>
                    </tr>
                    <tr>
                        <td>Ответы на вопросы по тематике обучения</td>
                        <td>Обучение без отрыва от производства</td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <div class="text__block studying-center__block">
                    <div class="text-block__header">Оснащение учебного класса</div>
                    <div class="text-block-wrap clearfix">
                        <div class="text-block__col">
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Современные компьютеры</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Безопасные ж/к мониторы</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Профессиональные проекторы</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Шумоизоляция учебных классов</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Система кондиционирования воздуха</span>
                            </div>
                        </div>
                        <div class="text-block__col">
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Вентиляция и очистка воздуха</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Антибактериальная очистка воздуха</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Кулер (очищенная питьевая вода)</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Инвентарь для кофе паузы</span>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon text-block__marker icon icon_checked"></span><span class="text-block__title text-block__title_offset">Парковка вблизи от входа</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="studying-center__btn-block">
                    <div class="btn btn_linear studying-center__btn">
                        <a href="#" class="link btn__link studying-center__btn-link" title="Задать вопрос">Задать вопрос</a>
                    </div>
                    <div class="btn btn_main studying-center__btn">
                        <a href="#" class="link btn__link studying-center__btn-link" title="Подобрать курс">Подобрать курс</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="status visibility">
        <div class="container container_narrow">
            <h2 class="status__header">Статусы учебного центра</h2>
            <div class="status__slider">
                <div class="status-slide-wrap">
                    <div class="status__slide clearfix">
                        <a href="/images/status__slide.png" class="link status-slide__img" title="1С:Центр Сертифицированного Обучения" data-fancybox="images"><span class="icon icon_zoom-in status-slide__img-pattern"></span></a>
                        <div class="status-slide__title-and-text">
                            <h3 class="status-slide__title">1С:Центр Сертифицированного Обучения</h3>
                            <div class="status-slide__text">Статус Центра Сертифицированного Обучения позволяет Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8», по курсам, разработанным фирмой «1С». Слушателю гарантируется качественное обучение, которое ведется сертифицированными преподавателями, по методическим материалам, подготовленным фирмой «1С». По окончанию курса пользователи получают свидетельства фирмы «1С». Подтверждено сертификатом фирмы 1С. </div>
                        </div>
                    </div>
                </div>
                <div class="status-slide-wrap">
                    <div class="status__slide clearfix">
                        <a href="/images/status__slide.png" class="link status-slide__img" title="1С:Центр Сертифицированного Обучения" data-fancybox="images"><span class="icon icon_zoom-in status-slide__img-pattern"></span></a>
                        <div class="status-slide__title-and-text">
                            <h3 class="status-slide__title">1С:Центр Сертифицированного Обучения</h3>
                            <div class="status-slide__text">Статус Центра Сертифицированного Обучения позволяет Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8», по курсам, разработанным фирмой «1С». Слушателю гарантируется качественное обучение, которое ведется сертифицированными преподавателями, по методическим материалам, подготовленным фирмой «1С». По окончанию курса пользователи получают свидетельства фирмы «1С». Подтверждено сертификатом фирмы 1С. </div>
                        </div>
                    </div>
                </div>
                <div class="status-slide-wrap">
                    <div class="status__slide clearfix">
                        <a href="/images/status__slide.png" class="link status-slide__img" title="1С:Центр Сертифицированного Обучения" data-fancybox="images"><span class="icon icon_zoom-in status-slide__img-pattern"></span></a>
                        <div class="status-slide__title-and-text">
                            <h3 class="status-slide__title">1С:Центр Сертифицированного Обучения</h3>
                            <div class="status-slide__text">Статус Центра Сертифицированного Обучения позволяет Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8», по курсам, разработанным фирмой «1С». Слушателю гарантируется качественное обучение, которое ведется сертифицированными преподавателями, по методическим материалам, подготовленным фирмой «1С». По окончанию курса пользователи получают свидетельства фирмы «1С». Подтверждено сертификатом фирмы 1С. </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="consul">
        <div class="container container_narrow">
            <h1 class="consul__header">Необходима консультация?</h1>
            <div class="consul__desc">Бесплатно проконсультироваться, уточнить цены и заказать решение можно у специалистов нашей фирмы</div>
            <form action="#" class="consul__form feedback-form clearfix">
                <div class="feedback-form__group clearfix">
                    <div class="consul__item">
                        <div class="feedback-form__group">
                            <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
                        </div>
                    </div>
                    <div class="consul__item">
                        <div class="feedback-form__group">
                            <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш телефон">
                        </div>
                    </div>
                    <div class="consul__item">
                        <div class="feedback-form__group">
                            <div class="feedback-form__control_type_select"><span class="feedback-form__control_type_current-option">Выберите город</span><span class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                            <ul class="feedback-form__control_type_options">
                                <li class="feedback-form__control_type_option">Город 1</li>
                                <li class="feedback-form__control_type_option">Город 2</li>
                                <li class="feedback-form__control_type_option">Город 3</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="feedback-form__group">
                    <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span class="btn__link">Получить консультацию</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                    </label>
                </div>
            </form>
        </div>
    </section>
</div>
<footer class="footer">
    <div class="footer__consul-form feedback-form-modal-wrap" id="consul-form">
        <div class="feedback-form__title
            <h1 class="feedback-form__title-valueНужна консультация?</h1>
            <div class="feedback-form__desc">Бесплатно проконсультироваться, уточнить цены и<br>заказать решение можно у специалистов нашей фирмы</div>
        </div>

        <form action="#" class="feedback-form feedback-form_modal">
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
            </div>
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш телефон">
            </div>
            <div class="feedback-form__group">
                <div class="feedback-form__control_type_select"><span class="feedback-form__control_type_current-option">Выберите город</span><span class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                <ul class="feedback-form__control_type_options">
                    <li class="feedback-form__control_type_option">Город 1</li>
                    <li class="feedback-form__control_type_option">Город 2</li>
                    <li class="feedback-form__control_type_option">Город 3</li>
                </ul>
            </div>
            <div class="feedback-form__group">
                <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
            </div>
            <div class="form__footer">
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                    </label>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span class="btn__link">Получить консультацию</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>
            </div>
        </form>

    </div>
    <div class="footer__feedback-form feedback-form-modal-wrap" id="feeedback-form">
        <div class="feedback-form__title
            <h1 class="feedback-form__title-valueНаписать нам</h1>
        </div>
        <form action="#" class="feedback-form feedback-form_modal">
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
            </div>
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш email">
            </div>
            <div class="feedback-form__group">
                <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
            </div>
            <div class="form__footer">
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                    </label>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span class="btn__link">Отправить сообщение</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>

            </div>
        </form>
    </div>
    <div class="footer__menu clearfix">
        <div class="container container_narrow">
            <div class="footer-menu__item">
                <div class="footer-menu__header">1С: Предприятие 8</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бухгалтерия 8">1С: Бухгалтерия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление торговлей">1С: Управление торговлей</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Зарплата и Управление персоналом">1С: Зарплата и Управление персоналом</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Комплексная автоматизация">1С: Комплексная автоматизация</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление производственным предприятием">1С: Управление<br>производственным предприятием</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление небольшой фирмой">1С: Управление небольшой<br>фирмой</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <div class="footer-menu__header">Для бюджетников</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бухгалтерия государственного учреждения">1С: Бухгалтерия<br>государственного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Зарплата и кадры бюджетного учреждения">1С: Зарплата и кадры<br>бюджетного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бюджетная отчетность">1С: Бюджетная отчетность</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Свод отчетов">1С: Свод отчетов</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Государственные и муниципальные закупки">1С: Государственные и муниципальные закупки</a></li>
                </ul>
            </div>
            <div class="footer-menu__item footer-menu__item_narrow">
                <div class="footer-menu__header">Услуги</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Консультации по выбору программного обеспечения">Консультации по выбору программного обеспечения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Доставка и установка">Доставка и установка</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Настройка и внедрение">Настройка и внедрение</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Учебный центр">1С: Учебный центр</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Электронная отчетность из 1С: Предприятия 8">Электронная отчетность из<br>1С: Предприятия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Центр сопровождение 1С">Центр сопровождение 1С</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <div class="footer-menu__header">О компании</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Статусы компании">Статусы компании</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Наши сотрудники">Наши сотрудники</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Вакансии">Вакансии</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Новости">Новости</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Фотогалерея">Фотогалерея</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Контакты">Контакты</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer__contact">
        <div class="container container_narrow">
            <div class="footer-contact__item">
                <div class="footer-contact__city">Пятигорск</div>
                <div class="footer-contact__tel">+7 (8793) 97-34-34 </div>
                <div class="footer-contact__address">ул. Коста Хетагурова 4 </div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Ставрополь</div>
                <div class="footer-contact__tel">+7 (8652) 301-103 </div>
                <div class="footer-contact__address">ул. Мира, 360</div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Черкесск</div>
                <div class="footer-contact__tel">+7 (8782) 26-00-44</div>
                <div class="footer-contact__address">ул. Кирова, 21а (4-этаж) </div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact-btn-wrap">
                    <div class="btn btn_main footer-contact__btn"><a href="#consul-form" class="link btn__link footer-contact__btn-link footer__consul-opener" title="Получить консультацию">Получить консультацию</a></div>
                    <div class="btn btn_linear footer-contact__btn"><a href="#feeedback-form" class="link btn__link footer-contact__btn-link footer__feedback-opener" title="Написать нам">Написать нам</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__signs">
        <div class="container container_narrow">
            <div class="footer__copyright">© 2002–2017. Все права защищены. </div>
            <div class="footer__dev-sign">
                <div class="footer-dev-sign__title">Создание сайта</div>
                <div class="footer-dev-sign__name">Студия Z-labs</div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="frontend/local/app.js"></script>
</body>
</html>