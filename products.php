<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="frontend/local/app.css">
    <title>Products</title>
</head>
<body class="body">
<header class="header">
    <div class="container container_narrow">
        <div class="header__item">
            <a href="#" class="header__brand header__logo" title="Главная"></a>
        </div>
        <div class="header__item">
            <div class="header__slogan">Простые решения<br>для вашего бизнеса</div>
        </div>
        <div class="header__item">
            <div class="header__location-and-tel">
                <div class="header__location">
                    <button class="header-location__btn header__title"><span class="header-location__showcase"><span class="header-location__link icon icon_arrow-angle-down">Пятигорск</span></span></button>
                    <div class="header-location__list">
                        <div class="header-location__item header-location__item_active">Пятигорск</div>
                        <div class="header-location__item">Ставрополь</div>
                        <div class="header-location__item">Черкесск</div>
                    </div>
                </div>
                <div class="header__text header__tel">+7 (8793) 97-34-34 </div>
            </div>
        </div>
        <div class="header__item">
            <div class="header__mail">
                <div class="header__title">Эл. почта</div>
                <div class="header__text">info@mskmv.ru</div>
            </div>
        </div>
        <div class="header__item">
            <a href="#" class="header__brand header__1c" title="1C:Центр Сопровождения"></a>
        </div>
    </div>
</header>
<nav class="nav">
    <div class="container container_narrow">
        <ul class="nav__list clearfix">
            <li class="nav__item">
                <a href="#" class="link nav__link" title="О нас"><span class="nav__undescore">О нас</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Продукты"><span class="nav__undescore">Продукты</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Услуги"><span class="nav__undescore">Услуги</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Сопровождение"><span class="nav__undescore">1С:Сопровождение</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Учебный центр"><span class="nav__undescore">1С:Учебный центр</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Отчетность"><span class="nav__undescore">1С:Отчетность</span></a>
            </li>
        </ul>
    </div>
</nav>
<div class="breadcrumbs">
    <div class="container container_narrow">
        <a href="#" class="link breadcrumbs__link icon icon_arrow-right" title="Главная">Главная</a>
        <span class="breadcrumbs__dest">Продукты</span>
    </div>
</div>
<div class="main clearfix">
    <div class="container container_narrow">
        <h1 class="h1">1С:Предприятие 8</h1>
        <div class="content">
            <div class="products text">
                <p>На сегодняшний день самые известные и продаваемые программы для автоматизации учета предлагает фирма «1С», и это неудивительно в свете неустанного совершенствования программных продуктов системы «1С:Предприятие». Фирмой разработаны типовые конфигурации для самых разных видов деятельности, значительно превышающие возможное количество типовых решений</p>
                <div class="text__block products-text-block products-text-block_tall">
                    <div class="text-block__header product__section">Предоставляемый спектр услуг</div>
                    <div class="text-block-wrap clearfix">
                        <div class="products-text-block__col">
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_count"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Регламентированный учет</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Бухгалтерия 8">1С:Бухгалтерия 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Зарплата и управление персоналом 8">1С:Зарплата и управление персоналом 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление корпоративными финансами">1С:Предприятие 8. Управление корпоративными финансами</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_elevator"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Торговый и складской учет</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Управление нашей фирмой 8">1С:Управление нашей фирмой 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Документооборот 8">1С:Документооборот 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Консолидация 8">1С:Консолидация 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управляющий">1С:Предприятие 8. Управляющий</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Договорчики 8">1С:Договорчики 8</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="products-text-block__col">
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_checklist"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Управленческий учет</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Управление торговлей 8">1С:Управление торговлей 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Розница 8">1С:Розница 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. CRM">1С:Предприятие 8. CRM</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_list"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Комплексные решения</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:ERP Управление предприятием">1С:ERP Управление предприятием</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С Комплексная автоматизация 8">1С Комплексная автоматизация 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Комплект прикладных решений на 5 пользователей">1С:Предприятие 8. Комплект прикладных решений на 5 пользователей</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Корпорация">1С:Корпорация</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Управление холдингом 8">1С:Управление холдингом 8</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text__block products-text-block">
                    <div class="text-block__header product__section">Отраслевые решения 1С</div>
                    <div class="text-block-wrap clearfix">
                        <div class="products-text-block__col">
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_dish"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Пищевая промышленность</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. 1С:Хлебобулочное и кондитерское производство 8">1С:Предприятие 8. 1С:Хлебобулочное и кондитерское производство 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Ликероводочный и винный завод">1С:Предприятие 8. Ликероводочный и винный завод</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Пиво-безалкогольный комбинат">1С:Предприятие 8. Пиво-безалкогольный комбинат</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Мясокомбинат">1С:Предприятие 8. Мясокомбинат</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Молокозавод">1С:Предприятие 8. Молокозавод</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Рыбопереработка">1С:Предприятие 8. Рыбопереработка</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_truck-italic"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Логистика и склад</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. 1С-Логистика. Управление складом">1С:Предприятие 8. 1С-Логистика. Управление складом</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление автотранспортом">1С:Предприятие 8.Управление автотранспортом</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:WMS Логистика.Управление складом">1С:WMS Логистика.Управление складом</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. TMS Логистика. Управление перевозками">1С:Предприятие 8. TMS Логистика. Управление перевозками</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление транспортным предприятием">1С:Предприятие 8. Управление транспортным предприятием</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_printer"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Полиграфия</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Полиграфия">1С:Предприятие 8. Полиграфия</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Оперативная печать">1С:Предприятие 8. Оперативная печать</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Широкоформатная печать">1С:Предприятие 8. Широкоформатная печать</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_gear2"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Другие отрасли</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Фармпроизводство">1С:Предприятие 8. Фармпроизводство</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1C:ERP Энергетика">1C:ERP Энергетика</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Горнодобывающая промышленность. Управление карьером">1С:Горнодобывающая промышленность. Управление карьером</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Комбинат ЖБИ">1С:Комбинат ЖБИ</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Аренда и управление недвижимостью">1С:Предприятие 8. Аренда и управление недвижимостью</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8 Спиртовое производство">1С:Предприятие 8 Спиртовое производство</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление водоканалом">1С:Предприятие 8. Управление водоканалом</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление предприятием ЖКХ">1С:Предприятие 8. Управление предприятием ЖКХ</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление теплосетью">1С:Предприятие 8. Управление теплосетью</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Процессное производство. Химия">1С:Процессное производство. Химия</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Риэлтор. Управление продажами недвижимости. ПРОФ">1С:Риэлтор. Управление продажами недвижимости. ПРОФ</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Управление металлургическим комбинатом">1С:Управление металлургическим комбинатом</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Управление переработкой отходов и вторсырья">1С:Управление переработкой отходов и вторсырья</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Энергетика. Управление распределительной сетевой компанией">1С:Энергетика. Управление распределительной сетевой компанией</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="Модуль 1С:Аренда и управление недвижимостью для 1С:ERP">Модуль 1С:Аренда и управление недвижимостью для 1С:ERP</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_tools"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Профессиональные услуги</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:SPA-Салон">1С:SPA-Салон</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Автосервис">1С:Автосервис</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Кадровое агентство">1С:Кадровое агентство</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Общепит 8">1С:Общепит 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Отель">1С:Предприятие 8. Отель</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Ресторан">1С:Предприятие 8. Ресторан</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Турагентство">1С:Предприятие 8. Турагентство</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление проектной организацией">1С:Предприятие 8. Управление проектной организацией</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление проектным офисом">1С:Предприятие 8. Управление проектным офисом</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление сервисным центром">1С:Предприятие 8. Управление сервисным центром</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Салон красоты">1С:Салон красоты</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Такси и аренда автомобилей">1С:Такси и аренда автомобилей</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Фитнес клуб">1С:Фитнес клуб</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Фотоуслуги">1С:Фотоуслуги</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="products-text-block__col">
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_trees"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Сельское и лесное хозяйство</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление птицефабрикой">1С:Предприятие 8. Управление птицефабрикой</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление сельскохозяйственным предприятием">1С:Предприятие 8. Управление сельскохозяйственным предприятием</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:ERP Агропромышленный комплекс 2">1С:ERP Агропромышленный комплекс 2</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Лесозавод">1С:Предприятие 8. Лесозавод</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление мукомольно-крупяным предприятием">1С:Предприятие 8. Управление мукомольно-крупяным предприятием</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_hook"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Строительство</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Бухгалтерия строительной организации">1С:Предприятие 8. Бухгалтерия строительной организации</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Подрядчик строительства 3.0">1С:Предприятие 8. Подрядчик строительства 3.0</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Подрядчик строительства 4.0">1С:Подрядчик строительства 4.0</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Смета">1С:Предприятие 8. Смета</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Производство строительных материалов">1С:Предприятие 8. Производство строительных материалов</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление строительной организацией">1С:Предприятие 8. Управление строительной организацией</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1C:Предприятие 8. Элит-строительство">1C:Предприятие 8. Элит-строительство</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:ERP Управление строительной организацией 2">1С:ERP Управление строительной организацией 2</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="Программы 1С для строительства">Программы 1С для строительства</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_case"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Управление ресурсами</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление производственным предприятием">1С:Предприятие 8. Управление производственным предприятием</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. PDM Управление инженерными данными">1С:Предприятие 8. PDM Управление инженерными данными</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:ERP+PM Управление проектной организацией 2">1С:ERP+PM Управление проектной организацией 2</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. ITIL Управление информационными технологиями предприятия">1С:Предприятие 8. ITIL Управление информационными технологиями предприятия</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. MDM Управление нормативно-справочной информацией">1С:Предприятие 8. MDM Управление нормативно-справочной информацией</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. MES Оперативное управление производством">1С:Предприятие 8. MES Оперативное управление производством</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. МТО Материально-техническое обеспечение">1С:Предприятие 8. МТО Материально-техническое обеспечение</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. ТОИР Управление ремонтами и обслуживанием оборудования">1С:Предприятие 8. ТОИР Управление ремонтами и обслуживанием оборудования</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление ремонтным предприятием">1С:Предприятие 8. Управление ремонтным предприятием</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:ТОИР Управление ремонтами и обслуживанием оборудования 2 КОРП">1С:ТОИР Управление ремонтами и обслуживанием оборудования 2 КОРП</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_building"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Государственные учреждения</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Свод отчетов 8 ПРОФ">1С:Свод отчетов 8 ПРОФ</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Бюджетная отчетность 8">1С:Бюджетная отчетность 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Бухгалтерия государственного учреждения 8">1С:Бухгалтерия государственного учреждения 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Государственные и муниципальные закупки 8">1С:Государственные и муниципальные закупки 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Документооборот государственного учреждения 8">1С:Документооборот государственного учреждения 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Зарплата и кадры бюджетного учреждения 8">1С:Зарплата и кадры бюджетного учреждения 8</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="11С:Зарплата и кадры государственного учреждения 8">11С:Зарплата и кадры государственного учреждения 8</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="text-block__item">
                                <span class="text-block__icon products-block__icon icon icon_cart2"></span>
                                <div class="products-text-block__title-and-list">
                                    <div class="products-text-block__title text-block__title">Торговые предприятия</div>
                                    <ul class="products-text-block__list">
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Комплексная автоматизация торговли алкогольной продукцией">1С:Комплексная автоматизация торговли алкогольной продукцией</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Мобильная торговля">1С:Мобильная торговля</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Предприятие 8. Управление торговлей алкогольной продукцией">1С:Предприятие 8. Управление торговлей алкогольной продукцией</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Розница 8. Аптека">1С:Розница 8. Аптека</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Розница 8. Книжный магазин">1С:Розница 8. Книжный магазин</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Розница 8. Магазин автозапчастей">1С:Розница 8. Магазин автозапчастей</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Розница 8. Магазин бытовой техники и средств связи">1С:Розница 8. Магазин бытовой техники и средств связи</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Розница 8. Магазин одежды и обуви">1С:Розница 8. Магазин одежды и обуви</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Розница 8. Магазин строительных и отделочных материалов">1С:Розница 8. Магазин строительных и отделочных материалов</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Розница 8. Салон оптики">1С:Розница 8. Салон оптики</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Розница 8. Ювелирный магазин">1С:Розница 8. Ювелирный магазин</a>
                                        </li>
                                        <li class="products-text-block__item">
                                            <a href="#" class="link products-text-block__link" title="1С:Управление аптечной сетью">1С:Управление аптечной сетью</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="products__vendors products-vendors_small">
                    <div class="products-vendors__title">Продукты других вендеров</div>
                    <div class="products-vendors__list clearfix">
                        <div class="products-vendors__logos clearfix">
                            <div class="products-vendors__row">
                                <div class="products-vendors-row__item">
                                    <div class="products-vendors-logo-wrap">
                                        <img class="products-vendors__logo" src="/images/products_vendors/11.png">
                                    </div>
                                </div>
                                <div class="products-vendors-row__item">
                                    <div class="products-vendors-logo-wrap">
                                        <img class="products-vendors__logo" src="/images/products_vendors/19.png">
                                    </div>
                                </div>
                                <div class="products-vendors-row__item">
                                    <div class="products-vendors-logo-wrap">
                                        <img class="products-vendors__logo" src="/images/products_vendors/7.png">
                                    </div>
                                </div>
                                <div class="products-vendors-row__item">
                                    <div class="products-vendors-logo-wrap">
                                        <img class="products-vendors__logo" src="/images/products_vendors/15.png">
                                    </div>
                                </div>
                            </div>
                            <div class="products-vendors__row">
                                <div class="products-vendors-row__item">
                                    <div class="products-vendors-logo-wrap">
                                        <img class="products-vendors__logo" src="/images/products_vendors/8.png">
                                    </div>
                                </div>
                                <div class="products-vendors-row__item">
                                    <div class="products-vendors-logo-wrap">
                                        <img class="products-vendors__logo" src="/images/products_vendors/14.png">
                                    </div>
                                </div>
                                <div class="products-vendors-row__item">
                                    <div class="products-vendors-logo-wrap">
                                        <img class="products-vendors__logo" src="/images/products_vendors/1.png">
                                    </div>
                                </div>
                                <div class="products-vendors-row__item">
                                    <div class="products-vendors-logo-wrap">
                                        <img class="products-vendors__logo" src="/images/products_vendors/9.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="products-vendors__footer">
                        <div class="btn btn_linear products-vendors__btn">
                            <a href="#" class="link btn__link products-vendors__btn-link" title="Остальные продукты">Остальные продукты</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="consul">
        <div class="container container_narrow">
            <h1 class="consul__header">Необходима консультация?</h1>
            <div class="consul__desc">Бесплатно проконсультироваться, уточнить цены и заказать решение можно у специалистов нашей фирмы</div>
            <form action="#" class="consul__form feedback-form clearfix">
                <div class="feedback-form__group clearfix">
                    <div class="consul__item">
                        <div class="feedback-form__group">
                            <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
                        </div>
                    </div>
                    <div class="consul__item">
                        <div class="feedback-form__group">
                            <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш телефон">
                        </div>
                    </div>
                    <div class="consul__item">
                        <div class="feedback-form__group">
                            <div class="feedback-form__control_type_select"><span class="feedback-form__control_type_current-option">Выберите город</span><span class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                            <ul class="feedback-form__control_type_options">
                                <li class="feedback-form__control_type_option">Город 1</li>
                                <li class="feedback-form__control_type_option">Город 2</li>
                                <li class="feedback-form__control_type_option">Город 3</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="feedback-form__group">
                    <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span class="btn__link">Получить консультацию</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                    </label>
                </div>
            </form>
        </div>
    </section>
</div>
<footer class="footer">
    <div class="footer__consul-form feedback-form-modal-wrap" id="consul-form">
        <div class="feedback-form__title">
            <h1 class="feedback-form__title-value">Нужна консультация?</h1>
            <div class="feedback-form__desc">Бесплатно проконсультироваться, уточнить цены и<br>заказать решение можно у специалистов нашей фирмы</div>
        </div>

        <form action="#" class="feedback-form feedback-form_modal">
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
            </div>
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш телефон">
            </div>
            <div class="feedback-form__group">
                <div class="feedback-form__control_type_select"><span class="feedback-form__control_type_current-option">Выберите город</span><span class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                <ul class="feedback-form__control_type_options">
                    <li class="feedback-form__control_type_option">Город 1</li>
                    <li class="feedback-form__control_type_option">Город 2</li>
                    <li class="feedback-form__control_type_option">Город 3</li>
                </ul>
            </div>
            <div class="feedback-form__group">
                <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
            </div>
            <div class="form__footer">
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                    </label>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span class="btn__link">Получить консультацию</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>
            </div>
        </form>

    </div>
    <div class="footer__feedback-form feedback-form-modal-wrap" id="feeedback-form">
        <div class="feedback-form__title">
            <h1 class="feedback-form__title-value">Написать нам</h1>
        </div>
        <form action="#" class="feedback-form feedback-form_modal">
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
            </div>
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш email">
            </div>
            <div class="feedback-form__group">
                <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
            </div>
            <div class="form__footer">
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                    </label>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span class="btn__link">Отправить сообщение</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>

            </div>
        </form>
    </div>
    <div class="footer__menu clearfix">
        <div class="container container_narrow">
            <div class="footer-menu__item">
                <div class="footer-menu__header">1С: Предприятие 8</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бухгалтерия 8">1С: Бухгалтерия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление торговлей">1С: Управление торговлей</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Зарплата и Управление персоналом">1С: Зарплата и Управление персоналом</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Комплексная автоматизация">1С: Комплексная автоматизация</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление производственным предприятием">1С: Управление<br>производственным предприятием</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление небольшой фирмой">1С: Управление небольшой<br>фирмой</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <div class="footer-menu__header">Для бюджетников</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бухгалтерия государственного учреждения">1С: Бухгалтерия<br>государственного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Зарплата и кадры бюджетного учреждения">1С: Зарплата и кадры<br>бюджетного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бюджетная отчетность">1С: Бюджетная отчетность</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Свод отчетов">1С: Свод отчетов</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Государственные и муниципальные закупки">1С: Государственные и муниципальные закупки</a></li>
                </ul>
            </div>
            <div class="footer-menu__item footer-menu__item_narrow">
                <div class="footer-menu__header">Услуги</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Консультации по выбору программного обеспечения">Консультации по выбору программного обеспечения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Доставка и установка">Доставка и установка</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Настройка и внедрение">Настройка и внедрение</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Учебный центр">1С: Учебный центр</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Электронная отчетность из 1С: Предприятия 8">Электронная отчетность из<br>1С: Предприятия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Центр сопровождение 1С">Центр сопровождение 1С</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <div class="footer-menu__header">О компании</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Статусы компании">Статусы компании</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Наши сотрудники">Наши сотрудники</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Вакансии">Вакансии</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Новости">Новости</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Фотогалерея">Фотогалерея</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Контакты">Контакты</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer__contact">
        <div class="container container_narrow">
            <div class="footer-contact__item">
                <div class="footer-contact__city">Пятигорск</div>
                <div class="footer-contact__tel">+7 (8793) 97-34-34 </div>
                <div class="footer-contact__address">ул. Коста Хетагурова 4 </div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Ставрополь</div>
                <div class="footer-contact__tel">+7 (8652) 301-103 </div>
                <div class="footer-contact__address">ул. Мира, 360</div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Черкесск</div>
                <div class="footer-contact__tel">+7 (8782) 26-00-44</div>
                <div class="footer-contact__address">ул. Кирова, 21а (4-этаж) </div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact-btn-wrap">
                    <div class="btn btn_main footer-contact__btn"><a href="#consul-form" class="link btn__link footer-contact__btn-link footer__consul-opener" title="Получить консультацию">Получить консультацию</a></div>
                    <div class="btn btn_linear footer-contact__btn"><a href="#feeedback-form" class="link btn__link footer-contact__btn-link footer__feedback-opener" title="Написать нам">Написать нам</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__signs">
        <div class="container container_narrow">
            <div class="footer__copyright">© 2002–2017. Все права защищены. </div>
            <div class="footer__dev-sign">
                <div class="footer-dev-sign__title">Создание сайта</div>
                <div class="footer-dev-sign__name">Студия Z-labs</div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="frontend/local/app.js"></script>
</body>
</html>