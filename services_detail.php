<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="frontend/local/app.css">
    <title>Detail</title>
</head>
<body class="body">
<header class="header">
    <div class="container container_narrow">
        <div class="header__item">
            <a href="#" class="header__brand header__logo" title="Главная"></a>
        </div>
        <div class="header__item">
            <div class="header__slogan">Простые решения<br>для вашего бизнеса</div>
        </div>
        <div class="header__item">
            <div class="header__location-and-tel">
                <div class="header__location">
                    <button class="header-location__btn header__title"><span class="header-location__showcase"><span class="header-location__link icon icon_arrow-angle-down">Пятигорск</span></span></button>
                    <div class="header-location__list">
                        <div class="header-location__item header-location__item_active">Пятигорск</div>
                        <div class="header-location__item">Ставрополь</div>
                        <div class="header-location__item">Черкесск</div>
                    </div>
                </div>
                <div class="header__text header__tel">+7 (8793) 97-34-34 </div>
            </div>
        </div>
        <div class="header__item">
            <div class="header__mail">
                <div class="header__title">Эл. почта</div>
                <div class="header__text">info@mskmv.ru</div>
            </div>
        </div>
        <div class="header__item">
            <a href="#" class="header__brand header__1c" title="1C:Центр Сопровождения"></a>
        </div>
    </div>
</header>
<nav class="nav">
    <div class="container container_narrow">
        <ul class="nav__list clearfix">
            <li class="nav__item">
                <a href="#" class="link nav__link" title="О нас"><span class="nav__undescore">О нас</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Продукты"><span class="nav__undescore">Продукты</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Услуги"><span class="nav__undescore">Услуги</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Сопровождение"><span class="nav__undescore">1С:Сопровождение</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Учебный центр"><span class="nav__undescore">1С:Учебный центр</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Отчетность"><span class="nav__undescore">1С:Отчетность</span></a>
            </li>
        </ul>
    </div>
</nav>
<div class="breadcrumbs">
    <div class="container container_narrow">
        <a href="#" class="link breadcrumbs__link icon icon_arrow-right" title="Главная">Главная</a>
        <a href="#" class="link breadcrumbs__link icon icon_arrow-right" title="Наши услуги">Наши услуги</a>
        <a href="#" class="link breadcrumbs__link icon icon_arrow-right" title="1C : Учебный центр">1C : Учебный центр</a>
        <a href="#" class="link breadcrumbs__link icon" title="Сертифицированные курсы фирмы “1С”">Сертифицированные курсы фирмы “1С”</a>
    </div>
</div>
<div class="main clearfix">
    <div class="container container_narrow">
        <div class="sidebar">
            <div class="sidebar__menu">
                <a href="#" class="link sidebar-menu__link sidebar-menu__link_active" title="1С : Учебный центр">1С : Учебный центр</a>
                <a href="#" class="link sidebar-menu__link" title="Электронная отчетность из 1С : Предприятия 8">Электронная отчетность<br>из 1С : Предприятия 8</a>
                <a href="#" class="link sidebar-menu__link" title="Центр сопровождение 1С, ИТС Информационно технологическое сопровождение">Центр сопровождение 1С, ИТС Информационно технологическое сопровождение</a>
                <a href="#" class="link sidebar-menu__link" title="Настройка и внедрение">Настройка и внедрение</a>
                <a href="#" class="link sidebar-menu__link" title="Консультации по выбору программного обеспечения">Консультации по выбору программного обеспечения</a>
                <a href="#" class="link sidebar-menu__link" title="Доставка и установка">Доставка и установка</a>
            </div>
            <div class="sidebar__banner sidebar__banner_v1"></div>
            <div class="sidebar__banner sidebar__banner_v2"></div>
        </div>
        <h1 class="h1">1С : Предприятие 8. Использование конфигурации Бухгалтерия предприятия. Пользовательские режимы</h1>
        <div class="content content_w-sidebar">
            <div class="services-detail text">
                <p>Курс предназначен для пользователей, знакомых с основами бухгалтерского и налогового учета, которые планируют использовать в своей работе прикладное решение "1С:Бухгалтерия 8" (другое название, более близкое специалистам в системе "1С:Предприятие"<br>конфигурация "Бухгалтерия предприятия") .</p>
                <div class="service-detail__block">
                    <h4>Цель данного курса</h4>
                    <ul>
                        <li>
                            <span>Изучение прикладного решения "1С:Бухгалтерия 8";</span>
                        </li>
                        <li>
                            <span>Приобретение целостного и правильного понимания работы "1С:Бухгалтерия 8" как инструмента для решения задач бухгалтерского и налогового учета;</span>
                        </li>
                        <li>
                            <span>Приобретение и закрепление базовых навыков работы с системой "1С:Предприятие 8".</span>
                        </li>
                    </ul>
                </div>
                <div class="service-detail__block">
                    <h4>План курса</h4>
                    <ul>
                        <li>
                            <span>Общая характеристика конфигурации</span>
                        </li>
                        <li>
                            <span>План счетов ЕСПБУ. Рабочий план счетов</span>
                        </li>
                        <li>
                            <span>Общие принципы работы с программой</span>
                        </li>
                        <li>
                            <span>Общие принципы ведения учета</span>
                        </li>
                        <li>
                            <span>Ввод начальных остатков</span>
                        </li>
                        <li>
                            <span>Санкционирование расходов</span>
                        </li>
                        <li>
                            <span>Расчетные документы</span>
                        </li>
                    </ul>
                </div>
                <div class="service-detail__block">
                    <h4>По итогам курса вы сможете</h4>
                    <ul>
                        <li>
                            <span>Готовить программу к эксплуатации;</span>
                        </li>
                        <li>
                            <span>Вводить начальные остатки;</span>
                        </li>
                        <li>
                            <span>Добавлять первичную информацию (платежки, ордера, контрагентов, накладные и т.д.);</span>
                        </li>
                        <li>
                            <span>Вести документооборот на каждом участке учета;</span>
                        </li>
                        <li>
                            <span>Вести учет расчетов по НДС;</span>
                        </li>
                        <li>
                            <span>Вести учет расчетов по налогу на прибыль в соответствии с ПБУ 18/02;</span>
                        </li>
                        <li>
                            <span>Вести учет доходов и расходов для целей налогообложения налогом на прибыль организаций;</span>
                        </li>
                        <li>
                            <span>формировать регламентированную отчетность;</span>
                        </li>
                        <li>
                            <span>получить престижный сертификат "1С:Профессионал".</span>
                        </li>
                    </ul>
                </div>
                <section class="service-detail__certificate">
                    <h2 class="service-detail-certificate__subheader">Сертификат об окончании курсов</h2>
                    <div class="service-detail-certificate__content clearfix">
                        <div class="service-detail-certificate__img"></div>
                        <div class="service-detail-certificate__text">
                            По окончании сертифицированного курса вы получите Сертификат о прохождении обучения, а при успешном тестировании, вы получите еще и сертификат «1С:ПРОФЕССИОНАЛ», подтверждающий, что вы являетесь сертифицированным специалистом по программе 1С.
                        </div>
                    </div>
                </section>
                <div class="service-detail-features clearfix">
                    <div class="service-detail__feature clearfix">
                        <span class="service-detail__icon icon icon_wallet"></span>
                        <span class="service-detail__values">
                                <span class="service-detail__value">Стоимость</span>
                                <span class="service-detail__value"><span class="service-detail__number">7 500 </span><span class="service-detail__currency">"</span></span>
                            </span>
                    </div>
                    <div class="service-detail__feature clearfix">
                        <span class="service-detail__icon icon icon_time"></span>
                        <span class="service-detail__values">
                                <span class="service-detail__value">Продолжительность</span>
                                <span class="service-detail__value"><span class="service-detail__time">6 </span><span class="service-detail__measure">часов</span></span>
                            </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container container_narrow">
        <section class="show-license clearfix">
            <div class="show-license__content  clearfix">
                <h2 class="show-license__header">Лицензия на обучение<br>и сертификацию студентов</h2>
                <div class="btn btn_main show-license__btn">
                    <a href="#" class="link btn__link show-license__btn-link" title="Посмотреть лицензию">Посмотреть лицензию</a>
                </div>
            </div>
        </section>
    </div>
    <section class="review student-review visibility">
        <div class="container container_narrow">
            <div class="review__head student-review__head">
                <h2 class="h2 review__header review__header_center">Отзывы наших учеников</h2>
            </div>
            <div class="review__body">
                <div class="review__slider">
                    <div class="review-slide-wrap">
                        <div class="review__slide">
                            <div class="review-slide__img"></div>
                            <div class="review-slide__title-and-text">
                                <div class="review-slide__title">Вирта Татьяна Александровна</div>
                                <div class="review-slide__text">Статус Центра Сертифицированного Обучения позволяет Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по курсам, разработанным фирмой «1С».</div>
                            </div>
                        </div>
                    </div>
                    <div class="review-slide-wrap">
                        <div class="review__slide">
                            <div class="review-slide__img"></div>
                            <div class="review-slide__title-and-text">
                                <div class="review-slide__title">Вирта Татьяна Александровна</div>
                                <div class="review-slide__text">Статус Центра Сертифицированного Обучения позволяет Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по курсам, разработанным фирмой «1С».</div>
                            </div>
                        </div>
                    </div>
                    <div class="review-slide-wrap">
                        <div class="review__slide">
                            <div class="review-slide__img"></div>
                            <div class="review-slide__title-and-text">
                                <div class="review-slide__title">Вирта Татьяна Александровна</div>
                                <div class="review-slide__text">Статус Центра Сертифицированного Обучения позволяет Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по курсам, разработанным фирмой «1С».</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="review__footer">
                <div class="btn btn_linear review__btn student-review__btn"><a href="#" class="link btn__link review__btn-link student-review__btn-link" title="Читать отзыв целиком">Читать отзыв целиком</a></div>
            </div>
        </div>
    </section>
    <section class="consul consul_short">
        <div class="container container_narrow">
            <h1 class="consul__header">Необходима консультация?</h1>
            <div class="consul__desc">Бесплатно проконсультироваться, уточнить цены и заказать решение можно у специалистов нашей фирмы</div>
            <form action="#" class="consul__form feedback-form clearfix">
                <div class="feedback-form__group clearfix">
                    <div class="consul__item">
                        <div class="feedback-form__group">
                            <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
                        </div>
                    </div>
                    <div class="consul__item">
                        <div class="feedback-form__group">
                            <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш телефон">
                        </div>
                    </div>
                    <div class="consul__item">
                        <div class="feedback-form__group">
                            <div class="feedback-form__control_type_select"><span class="feedback-form__control_type_current-option">Выберите город</span><span class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                            <ul class="feedback-form__control_type_options">
                                <li class="feedback-form__control_type_option">Город 1</li>
                                <li class="feedback-form__control_type_option">Город 2</li>
                                <li class="feedback-form__control_type_option">Город 3</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="feedback-form__group">
                    <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span class="btn__link">Получить консультацию</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                    </label>
                </div>
            </form>
        </div>
    </section>
    <section class="similar visibility">
        <div class="similar__head">
            <div class="container container_narrow">
                <h2 class="similar__header">Вы можете посетить другие наши курсы</h2>
                <div class="similar-slider__toolbar clearfix">
                </div>
            </div>
        </div>
        <div class="similar__body">
            <div class="similar__slider">
                <div class="similar-slide-wrap">
                    <div class="similar__slide">
                        <a href="#" class="link similar-slide__title" title='1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"'>1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"</a>
                        <div class="similar-slide__value">Стоимость курса</div>
                        <div class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></div>
                    </div>
                </div>
                <div class="similar-slide-wrap">
                    <div class="similar__slide">
                        <a href="#" class="link similar-slide__title" title='Основы программирования в «1С: Предприятие 8» для школьников'>Основы программированияв «1С: Предприятие 8» для школьников</a>
                        <div class="similar-slide__value">Стоимость курса</div>
                        <div class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></div>
                    </div>
                </div>
                <div class="similar-slide-wrap">
                    <div class="similar__slide">
                        <a href="#" class="link similar-slide__title" title='1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"'>1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"</a>
                        <div class="similar-slide__value">Стоимость курса</div>
                        <div class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></div>
                    </div>
                </div>
                <div class="similar-slide-wrap">
                    <div class="similar__slide">
                        <a href="#" class="link similar-slide__title" title='1С:Предприятие 8. Управление торговлей". Практическое применение типовой конфигурации'>1С:Предприятие 8. Управление торговлей". Практическое применение типовой конфигурации</a>
                        <div class="similar-slide__value">Стоимость курса</div>
                        <div class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></div>
                    </div>
                </div>
                <div class="similar-slide-wrap">
                    <div class="similar__slide">
                        <a href="#" class="link similar-slide__title" title='1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"'>1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"</a>
                        <div class="similar-slide__value">Стоимость курса</div>
                        <div class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></div>
                    </div>
                </div>
                <div class="similar-slide-wrap">
                    <div class="similar__slide">
                        <a href="#" class="link similar-slide__title" title='"Ведение бюджетного учета в программе "1С:Бухгалтерия государственного учреждения'>"Ведение бюджетного учета в программе "1С:Бухгалтерия государственного учреждения</a>
                        <div class="similar-slide__value">Стоимость курса</div>
                        <div class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></div>
                    </div>
                </div>
                <div class="similar-slide-wrap">
                    <div class="similar__slide">
                        <a href="#" class="link similar-slide__title" title='1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"'>1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"</a>
                        <div class="similar-slide__value">Стоимость курса</div>
                        <div class="similar-slide__price">12 000<span class="similar-slide__currency">"</span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="similar__footer">
            <div class="container container_narrow">
                <div class="btn btn_linear similar__btn"><a href="#" class="link btn__link similar__btn-link" title="Все курсы">Все курсы</a></div>
                <div class="btn btn_main similar__btn"><a href="#" class="link btn__link similar__btn-link" title="Подобрать курс">Подобрать курс</a></div>
            </div>
        </div>
    </section>
</div>
<footer class="footer">
    <div class="footer__consul-form feedback-form-modal-wrap" id="consul-form">
        <div class="feedback-form__title
            <h1 class="feedback-form__title-valueНужна консультация?</h1>
            <div class="feedback-form__desc">Бесплатно проконсультироваться, уточнить цены и<br>заказать решение можно у специалистов нашей фирмы</div>
        </div>

        <form action="#" class="feedback-form feedback-form_modal">
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
            </div>
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш телефон">
            </div>
            <div class="feedback-form__group">
                <div class="feedback-form__control_type_select"><span class="feedback-form__control_type_current-option">Выберите город</span><span class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                <ul class="feedback-form__control_type_options">
                    <li class="feedback-form__control_type_option">Город 1</li>
                    <li class="feedback-form__control_type_option">Город 2</li>
                    <li class="feedback-form__control_type_option">Город 3</li>
                </ul>
            </div>
            <div class="feedback-form__group">
                <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
            </div>
            <div class="form__footer">
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                    </label>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span class="btn__link">Получить консультацию</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>
            </div>
        </form>

    </div>
    <div class="footer__feedback-form feedback-form-modal-wrap" id="feeedback-form">
        <div class="feedback-form__title
            <h1 class="feedback-form__title-valueНаписать нам</h1>
        </div>
        <form action="#" class="feedback-form feedback-form_modal">
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
            </div>
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш email">
            </div>
            <div class="feedback-form__group">
                <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
            </div>
            <div class="form__footer">
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                    </label>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span class="btn__link">Отправить сообщение</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>

            </div>
        </form>
    </div>
    <div class="footer__menu clearfix">
        <div class="container container_narrow">
            <div class="footer-menu__item">
                <div class="footer-menu__header">1С: Предприятие 8</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бухгалтерия 8">1С: Бухгалтерия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление торговлей">1С: Управление торговлей</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Зарплата и Управление персоналом">1С: Зарплата и Управление персоналом</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Комплексная автоматизация">1С: Комплексная автоматизация</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление производственным предприятием">1С: Управление<br>производственным предприятием</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление небольшой фирмой">1С: Управление небольшой<br>фирмой</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <div class="footer-menu__header">Для бюджетников</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бухгалтерия государственного учреждения">1С: Бухгалтерия<br>государственного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Зарплата и кадры бюджетного учреждения">1С: Зарплата и кадры<br>бюджетного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бюджетная отчетность">1С: Бюджетная отчетность</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Свод отчетов">1С: Свод отчетов</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Государственные и муниципальные закупки">1С: Государственные и муниципальные закупки</a></li>
                </ul>
            </div>
            <div class="footer-menu__item footer-menu__item_narrow">
                <div class="footer-menu__header">Услуги</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Консультации по выбору программного обеспечения">Консультации по выбору программного обеспечения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Доставка и установка">Доставка и установка</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Настройка и внедрение">Настройка и внедрение</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Учебный центр">1С: Учебный центр</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Электронная отчетность из 1С: Предприятия 8">Электронная отчетность из<br>1С: Предприятия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Центр сопровождение 1С">Центр сопровождение 1С</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <div class="footer-menu__header">О компании</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Статусы компании">Статусы компании</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Наши сотрудники">Наши сотрудники</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Вакансии">Вакансии</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Новости">Новости</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Фотогалерея">Фотогалерея</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Контакты">Контакты</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer__contact">
        <div class="container container_narrow">
            <div class="footer-contact__item">
                <div class="footer-contact__city">Пятигорск</div>
                <div class="footer-contact__tel">+7 (8793) 97-34-34 </div>
                <div class="footer-contact__address">ул. Коста Хетагурова 4 </div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Ставрополь</div>
                <div class="footer-contact__tel">+7 (8652) 301-103 </div>
                <div class="footer-contact__address">ул. Мира, 360</div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Черкесск</div>
                <div class="footer-contact__tel">+7 (8782) 26-00-44</div>
                <div class="footer-contact__address">ул. Кирова, 21а (4-этаж) </div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact-btn-wrap">
                    <div class="btn btn_main footer-contact__btn"><a href="#consul-form" class="link btn__link footer-contact__btn-link footer__consul-opener" title="Получить консультацию">Получить консультацию</a></div>
                    <div class="btn btn_linear footer-contact__btn"><a href="#feeedback-form" class="link btn__link footer-contact__btn-link footer__feedback-opener" title="Написать нам">Написать нам</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__signs">
        <div class="container container_narrow">
            <div class="footer__copyright">© 2002–2017. Все права защищены. </div>
            <div class="footer__dev-sign">
                <div class="footer-dev-sign__title">Создание сайта</div>
                <div class="footer-dev-sign__name">Студия Z-labs</div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="frontend/local/app.js"></script>
</body>
</html>