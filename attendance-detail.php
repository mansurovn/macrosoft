<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="frontend/local/app.css">
    <title>Attendance detail</title>
</head>
<body class="body">
<header class="header">
    <div class="container container_narrow">
        <div class="header__item">
            <a href="#" class="header__brand header__logo" title="Главная"></a>
        </div>
        <div class="header__item">
            <div class="header__slogan">Простые решения<br>для вашего бизнеса</div>
        </div>
        <div class="header__item">
            <div class="header__location-and-tel">
                <div class="header__location">
                    <button class="header-location__btn header__title"><span class="header-location__showcase"><span
                                    class="header-location__link icon icon_arrow-angle-down">Пятигорск</span></span>
                    </button>
                    <div class="header-location__list">
                        <div class="header-location__item header-location__item_active">Пятигорск</div>
                        <div class="header-location__item">Ставрополь</div>
                        <div class="header-location__item">Черкесск</div>
                    </div>
                </div>
                <div class="header__text header__tel">+7 (8793) 97-34-34</div>
            </div>
        </div>
        <div class="header__item">
            <div class="header__mail">
                <div class="header__title">Эл. почта</div>
                <div class="header__text">info@mskmv.ru</div>
            </div>
        </div>
        <div class="header__item">
            <a href="#" class="header__brand header__1c" title="1C:Центр Сопровождения"></a>
        </div>
    </div>
</header>
<nav class="nav">
    <div class="container container_narrow">
        <ul class="nav__list clearfix">
            <li class="nav__item">
                <a href="#" class="link nav__link" title="О нас"><span class="nav__undescore">О нас</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Продукты"><span class="nav__undescore">Продукты</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Услуги"><span class="nav__undescore">Услуги</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Сопровождение"><span class="nav__undescore">1С:Сопровождение</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Учебный центр"><span class="nav__undescore">1С:Учебный центр</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Отчетность"><span
                            class="nav__undescore">1С:Отчетность</span></a>
            </li>
        </ul>
    </div>
</nav>
<div class="breadcrumbs">
    <div class="container container_narrow">
        <a href="#" class="link breadcrumbs__link icon icon_arrow-right" title="Главная">Главная</a>
        <span class="breadcrumbs__dest">Сервисы</span>
    </div>
</div>
<div class="main clearfix">
    <div class="container container_narrow">
        <h1 class="h1">1С-ЭДО</h1>
        <div class="content">
            <div class="attendance-detail text">
                <div class="attendance-detail__cats">В тарифах: <a href="/its.php" class="link attendance-detail-cats__cat" title="оптимальный">оптимальный</a>, <a href="/its.php" class="link attendance-detail-cats__cat" title="расширенный">расширенный</a></div>
                <p>1С: ЭДО — это программа (сервис), позволяющая совершать практически моментальный обмен юридически
                    значимыми документами (актами, счетами-фактурами и любыми другими видами документов) с вашими
                    контрагентами.
                </p>
                <div class="text__snippet">
                    <h4>Основной функционал</h4>
                    <ul>
                        <li><span>Мгновенная передача первичных документов в любую точку мира</span></li>
                        <li><span>Снижение рисков возникновения штрафов из-за ошибок при сдаче отчетности</span></li>
                        <li><span>Снижение затрат на расходные материалы</span></li>
                        <li><span>Быстрый поиск электронных документов</span></li>
                    </ul>
                </div>
                <div class="text__snippet">
                    <h4>Что входит в состав продукта</h4>
                    <ul>
                        <li><span>Официальный и быстрый способ получения лицензионных и самых актуальныхобновлений с ресурса (сервера) 1С.</span>
                        </li>
                        <li><span>Линия консультаций разработчика отраслевых решений сможет оперативно решить проблемы профильного характера. </span>
                        </li>
                        <li><span>Многочисленные каналы связи позволят оперативно задавать вопросы и получать развернутые ответы от ведущих специалистов разработчика программных продуктов. </span>
                        </li>
                        <li><span>Доступ к накопительным базам знаний для отраслевых решений, составленных из наиболее часто задаваемых вопросов и ответов на них, рекомендаций методистов для максимально полезного использования всего функционала нестандартной конфигурации 1С. </span>
                        </li>
                    </ul>
                </div>
                <h2>Условия подключения</h2>
                <p>Внимание! Подключение 1С: ИТС Отраслевой возможно только при наличии договора регулярного
                    сопровождения <a href="#" title="1С: ИТС">1С: ИТС</a>.</p>
                <p>Лицензионная поддержка отраслевых программных продуктов осуществляется только при наличии
                    активированного сервиса. </p>
                <div class="text__snippet">
                    <h4>Программы, в которых реализован сервис</h4>
                    <p>Продукты «1С:Облачный архив» могут быть встроены в любое приложение на платформе «1С:Предприятие
                        8» (начиная с версии платформы 8.2).</p>
                    <p>Программы, в которые сервис встроен по умолчанию:</p>
                    <ul>
                        <li><span>1С:Бухгалтерия предприятия 8 (с версии 3.0.44);</span>
                        </li>
                        <li><span>1С:Управление небольшой фирмой 8 (с версии 1.6.7.4).</span>
                        </li>
                    </ul>
                    <p>Самостоятельно встроить «1С: Облачный архив» в конфигурации на платформе «1С: Предприятие 8»
                        (начиная с версии 8.3.8.1652) возможно при помощи типовой конфигурации «1С: Библиотека
                        интернет-поддержки пользователей» (начиная с версии 2.1.8) .</p>
                </div>
                <h2>Преимущества</h2>
                <div class="accordion visibility">
                    <div class="accordion__item accordion__anchor icon icon_arrow-angle-down accordion-item__icon"
                         data-collapse-id="1">
                        <div class="accordion-item__text accordion__target">
                            В случае утраты данных, восстановить копию из облачного хранилища существенно прощеи
                            быстрее по сравнению с «ручным» восстановлением, а это экономит время и деньги.
                        </div>
                    </div>
                    <div class="accordion__item accordion__anchor icon icon_arrow-angle-down accordion-item__icon"
                         data-collapse-id="2">
                        <div class="accordion-item__text accordion__target">
                            В случае утраты данных, восстановить копию из облачного хранилища существенно прощеи быстрее по
                            сравнению с «ручным» восстановлением, а это экономит время и деньги.
                        </div>
                    </div>
                    <div class="accordion__item accordion__anchor icon icon_arrow-angle-down accordion-item__icon"
                         data-collapse-id="3">
                        <div class="accordion-item__text accordion__target">
                            В случае утраты данных, восстановить копию из облачного хранилища существенно прощеи быстрее по
                            сравнению с «ручным» восстановлением, а это экономит время и деньги.
                        </div>
                    </div>
                    <div class="accordion__item accordion__anchor icon icon_arrow-angle-down accordion-item__icon"
                         data-collapse-id="4">
                        <div class="accordion-item__text accordion__target">
                            В случае утраты данных, восстановить копию из облачного хранилища существенно прощеи быстрее по
                            сравнению с «ручным» восстановлением, а это экономит время и деньги.
                        </div>
                    </div>
                </div>
                <h2>Для кого данный продукт</h2>
                <div class="accordion accordion_grey visibility">
                    <div class="accordion__item accordion__anchor icon icon_arrow-angle-down accordion-item__icon"
                         data-hide>
                        <div class="accordion-item__title">Бухгалтеру, сотрудникам финансовых служб</div>
                        <div class="accordion-item__text accordion__target accordion__hided">
                            <p>Много времени тратится на ручной ввод данных в учетную систему?</p>
                            <p>По данным электронного документа автоматически создается поступление товаров и услуг,
                                которое вам остается только провести.</p>
                            <p>Часто возникают ошибки при ручном вводе?</p>
                            <p>Использование электронных документов избавляет вас от необходимости вручную вводить
                                данные в учетную систему.</p>
                            <p>Длительное время требуется на сбор счетов-фактур, происходят задержки с возмещением
                                НДС?</p>
                            <p>С электронным документооборотом сбор документов происходитв кратчайшие сроки.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion accordion_grey">
                    <div class="accordion__item accordion__anchor icon icon_arrow-angle-down accordion-item__icon"
                         data-hide>
                        <div class="accordion-item__title">Менеджеру по продажам</div>
                        <div class="accordion-item__text accordion__target accordion__hided">
                            <p>Много времени тратится на ручной ввод данных в учетную систему?</p>
                            <p>По данным электронного документа автоматически создается поступление товаров и услуг,
                                которое вам остается только провести.</p>
                            <p>Часто возникают ошибки при ручном вводе?</p>
                            <p>Использование электронных документов избавляет вас от необходимости вручную вводить
                                данные в учетную систему.</p>
                            <p>Длительное время требуется на сбор счетов-фактур, происходят задержки с возмещением
                                НДС?</p>
                            <p>С электронным документооборотом сбор документов происходитв кратчайшие сроки.</p>
                        </div>
                    </div>
                </div>
                <div class="accordion accordion_grey">
                    <div class="accordion__item accordion__anchor icon icon_arrow-angle-down accordion-item__icon"
                         data-hide>
                        <div class="accordion-item__title">Директору</div>
                        <div class="accordion-item__text accordion__target accordion__hided">
                            <p>Много времени тратится на ручной ввод данных в учетную систему?</p>
                            <p>По данным электронного документа автоматически создается поступление товаров и услуг,
                                которое вам остается только провести.</p>
                            <p>Часто возникают ошибки при ручном вводе?</p>
                            <p>Использование электронных документов избавляет вас от необходимости вручную вводить
                                данные в учетную систему.</p>
                            <p>Длительное время требуется на сбор счетов-фактур, происходят задержки с возмещением
                                НДС?</p>
                            <p>С электронным документооборотом сбор документов происходитв кратчайшие сроки.</p>
                        </div>
                    </div>
                </div>
                <h2>Цены на сервис</h2>
                <table class="attendance-detail__table attendance-detail__table_first-col-long">
                    <tbody>
                    <tr>
                        <th>Количество месяцев</th>
                        <th>Стоимость</th>
                    </tr>
                    <tr>
                        <td>2 месяца</td>
                        <td>3 100 <span class="attendance-detail__currency">й</span></td>
                    </tr>
                    <tr>
                        <td>3 месяца</td>
                        <td>4 580 <span class="attendance-detail__currency">й</span></td>
                    </tr>
                    <tr>
                        <td>4 месяца</td>
                        <td>6 010 <span class="attendance-detail__currency">й</span></td>
                    </tr>
                    <tr>
                        <td>5 месяца</td>
                        <td>7 390 <span class="attendance-detail__currency">й</span></td>
                    </tr>
                    <tr>
                        <td>6 месяца</td>
                        <td>8 730 <span class="attendance-detail__currency">й</span></td>
                    </tr>
                    <tr>
                        <td>7 месяца</td>
                        <td>10 020 <span class="attendance-detail__currency">й</span></td>
                    </tr>
                    </tbody>
                </table>
                <div class="attendance-detail__panels clearfix visibility">
                    <div class="attendance-detail__panel">
                        <div class="attendance-detail-panel-title__container">
                            <div class="attendance-detail-panel__title">1СПАРК Риски</div>
                        </div>
                        <div class="attendance-detail-panel__duration">
                            <span class="attendance-detail-panel-duration__text">При оплате за </span>
                            <button class="attendance-detail-panel__variant pop-up__trigger icon icon_arrow-angle-down">
                                <span class="attendance-detail-panel-variant__text">
                                    <span class="attendance-detail-panel-variant__current pop-up-list__current">12 месяцев</span>
                                    <span class="pop-up pop-up-list-wrap pop-up-list-wrap_mini pop-up_under">
                                        <span class="pop-up__list">
                                            <span class="pop-up-list__item" data-price="1 000">1 месяц</span>
                                            <span class="pop-up-list__item" data-price="3 000">3 месяца</span>
                                            <span class="pop-up-list__item" data-price="5 000">6 месяцев</span>
                                            <span class="pop-up-list__item pop-up-list__item_active" data-price="12 000">12 месяцев</span>
                                            <span class="pop-up-list__item" data-price="24 000">24 месяца</span>
                                        </span>
                                    </span>
                                </span>
                            </button>
                        </div>
                        <div class="attendance-detail-panel-price__container">
                            <div class="attendance-detail-panel__price">
                                <span class="attendance-detail-panel-price__value">16 264</span>
                                <span class="attendance-detail-panel-price__currency">"</span>
                            </div>
                        </div>
                        <div class="btn btn_linear attendance-detail-panel__btn">
                            <a href="#" class="link btn btn__link attendance-detail-panel__btn-link"
                               title="Оставить заявку">Оставить
                                заявку</a>
                        </div>
                        <a href="#" class="link attendance-detail-panel-about__link"
                           title="Подробности"><span class="attendance-detail-panel-about__underline">Подробности</span></a>
                    </div>
                    <div class="attendance-detail__panel">
                        <div class="attendance-detail-panel-title__container">
                            <div class="attendance-detail-panel__title">1СПАРК Риски +</div>
                        </div>
                        <div class="attendance-detail-panel__duration">
                            <span class="attendance-detail-panel-duration__text">При оплате за </span>
                            <button class="attendance-detail-panel__variant pop-up__trigger icon icon_arrow-angle-down">
                                <span class="attendance-detail-panel-variant__text">
                                    <span class="attendance-detail-panel-variant__current pop-up-list__current">12 месяцев</span>
                                    <span class="pop-up pop-up-list-wrap pop-up-list-wrap_mini pop-up_under">
                                        <span class="pop-up__list">
                                            <span class="pop-up-list__item">1 месяц</span>
                                            <span class="pop-up-list__item">3 месяца</span>
                                            <span class="pop-up-list__item">6 месяцев</span>
                                            <span class="pop-up-list__item pop-up-list__item_active">12 месяцев</span>
                                            <span class="pop-up-list__item">24 месяца</span>
                                        </span>
                                    </span>
                                </span>
                            </button>
                        </div>
                        <div class="attendance-detail-panel-price__container">
                            <div class="attendance-detail-panel__price">
                                <span class="attendance-detail-panel-price__value">16 264</span>
                                <span class="attendance-detail-panel-price__currency">"</span>
                            </div>
                        </div>
                        <div class="btn btn_linear attendance-detail-panel__btn">
                            <a href="#" class="link btn btn__link attendance-detail-panel__btn-link"
                               title="Оставить заявку">Оставить
                                заявку</a>
                        </div>
                        <a href="#" class="link attendance-detail-panel-about__link"
                           title="Подробности"><span class="attendance-detail-panel-about__underline">Подробности</span></a>
                    </div>
                    <div class="attendance-detail__panel">
                        <div class="attendance-detail-panel-title__container">
                            <div class="attendance-detail-panel__title">1СПАРК Риски + +</div>
                        </div>
                        <div class="attendance-detail-panel__duration">
                            <span class="attendance-detail-panel-duration__text">При оплате за </span>
                            <button class="attendance-detail-panel__variant pop-up__trigger icon icon_arrow-angle-down">
                                <span class="attendance-detail-panel-variant__text">
                                    <span class="attendance-detail-panel-variant__current pop-up-list__current">12 месяцев</span>
                                    <span class="pop-up pop-up-list-wrap pop-up-list-wrap_mini pop-up_under">
                                        <span class="pop-up__list">
                                            <span class="pop-up-list__item">1 месяц</span>
                                            <span class="pop-up-list__item">3 месяца</span>
                                            <span class="pop-up-list__item">6 месяцев</span>
                                            <span class="pop-up-list__item pop-up-list__item_active">12 месяцев</span>
                                            <span class="pop-up-list__item">24 месяца</span>
                                        </span>
                                    </span>
                                </span>
                            </button>
                        </div>
                        <div class="attendance-detail-panel-price__container">
                            <div class="attendance-detail-panel__price">
                                <span class="attendance-detail-panel-price__value">16 264</span>
                                <span class="attendance-detail-panel-price__currency">"</span>
                            </div>
                        </div>
                        <div class="btn btn_linear attendance-detail-panel__btn">
                            <a href="#" class="link btn btn__link attendance-detail-panel__btn-link"
                               title="Оставить заявку">Оставить
                                заявку</a>
                        </div>
                        <a href="#panel-table1" class="link attendance-detail-panel-about__link"
                           title="Подробности"><span class="attendance-detail-panel-about__underline">Подробности</span></a>
                        <div class="attendance-detail-panel__modal" id="panel-table1">
                            <div class="h1 attendance-detail-panel-modal__title">1СПАРК Риски</div>
                            <table class="attendance-detail__table attendance-detail__table_first-col-long attendance-detail__table_modal">
                                <tbody>
                                    <tr>
                                        <th>Вид работ</th>
                                        <th>Периодичность</th>
                                    </tr>
                                    <tr>
                                        <td>Отправка регламентированной отчетности через Интернет в электронномвиде (1С-Отчетность), в т.ч. сервис хранения электронной подписи в облаке</td>
                                        <td>Одно приложение на выбор</td>
                                    </tr>
                                    <tr>
                                        <td>Количество доступных приложений (программ)</td>
                                        <td>Любое (без ограничений)</td>
                                    </tr>
                                    <tr>
                                        <td>Количество информационных баз</td>
                                        <td>Без ограничений</td>
                                    </tr>
                                    <tr>
                                        <td>Количество сеансов (одновременно работающих пользователей, т.е.количество открытых окон браузера и тонкого клиента)</td>
                                        <td>Одно приложение на выбор</td>
                                    </tr>
                                    <tr>
                                        <td>Подключение дополнительных сеансов</td>
                                        <td>Без ограничений (за дополнительную плату)</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <h2>Видеообзор</h2>
                <div class="attendance-detail__video visibility">
                    <iframe width="825" height="400" src="https://www.youtube.com/embed/tFcnxXnTZjc?rel=0"
                            frameborder="0" allowfullscreen></iframe>
                </div>
                <h2>Как подключить «1С-Отчетность»</h2>
                <div class="text__snippet">
                    <ul>
                        <li><span>Подайте заявку на подключение «1С-Отчетности» на сайте, по телефону +7 495 241-61-21 или,
                            если вы уже являетесь нашим клиентом, обратитесь к своему персональному менеджеру по
                            электронной почте.</span>
                        </li>
                        <li><span><b>Мы выставляем вам счет:</b><br>
                            Если у вас нет действующей подписки на «1С: ИТС ПРОФ», то необходимо
                            будет оплатить полную стоимость подключения и использования сервиса «1С-Отчетность».</span>
                        </li>
                        <li><span>Если подписка есть и у вас всего одно юр. лицо или ИП, то сервис сдачи отчетности будет
                            предоставлен бесплатно. При наличии нескольких юр. лиц стоимость будет уменьшена.</span>
                        </li>
                        <li><span>Услуги по настройке сервиса необходимо будет оплатить.</span></li>
                        <li><span>После оплаты счета наши специалисты настроят вашу «1С» для работы с сервисом «1С-Отчетность»
                            и отправят заявление на подключение прямо из программы «1С» для регистрации в контролирующих
                            органах. Настройка будет проведена через удаленное подключения для ускорения и экономии
                            вашего времени и средств. Если вы желаете, чтобы работа была выполнена очно — у вас в офисе
                            и в вашем присутствии — можно вызвать специалиста.</span>
                        </li>
                        <li><span>Примерно через сутки придет ответ из контролирующих органов. Наш сотрудник вновь подключится
                            удаленно, получит удостоверяющий сертификат (он будет вашей цифровой подписью и нужен для
                            безопасности, чтобы никто не мог подделать вашу отчетность и сдать ее за вас) и произведет
                            финальную настройку программы «1С» для работы с «1С-Отчетностью».</span>
                        </li>
                        <li><span>Чтобы вы могли сразу приступить к работе с новым сервисом, наш сотрудник сразу проведет
                            обучение пользователей. Обычно это требует небольших затрат времени — 15-20 минут.</span>
                        </li>
                        <li><span>При необходимости вы можете сразу попросить специалиста «1С-Рарус» выполнить какие-то
                            дополнительные работы.</span>
                        </li>
                    </ul>
                </div>
                <h2>При подключении сервиса необходимо обратить внимание на следующие особенности</h2>
                <p>Для каждой организации, которой требуется сдавать электронную отчетность, нужно приобретать лицензию
                    на «1С-Отчетность» (ПП «Астрал отчетность») и создавать учетную запись в сервисе. Даже если вы
                    ведете учет по нескольким организациям в одной программе «1С». </p>
                <p>Для обособленных подразделений, подключив всего одну лицензию «1С-Отчетности», можно сдавать
                    отчетность в неограниченное количество отделений Федеральной налоговой службы (ФНС) .</p>
                <p>Используя «1С-Отчетность» дважды в год можно получить цифровую подпись при смене реквизитов. </p>
                <p>Если возникнет потребность отправить отчетность из какой-то бухгалтерской программы, не относящейся к
                    семейству «1С», то можно использовать функцию импорта внешних файлов в Вашем 1С: Предприятии и затем
                    отправить данные в контролирующие органы привычным способом черезинтернет применив 1С-ку. </p>
                <h2>Список документов, которые требуются дляподключения «1С-Отчетности»</h2>
                <ol class="text__numbered-list">
                    <li>Заявление на выдачу и регистрацию ключа подписи (распечатывается из ПП 1С), заверенное подписью
                        руководителя и печатью организации.
                    </li>
                    <li>Копия паспорта гражданина РФ, пользователя ЭП (т.е. руководителя организации).</li>
                    <li>Копия СНИЛС руководителя организации.</li>
                    <li>Копия документа, подтверждающий должность руководителя организации (Приказ о назначении на
                        должность).
                    </li>
                    <li>Копия свидетельства о постановке на учет в налоговый орган (Свидетельство о регистрации ЮЛ).
                    </li>
                </ol>
                <blockquote>На семинаре рассматриваются особенности составления отчетности за 2014 год казенными,
                    бюджетными и автономными учреждениям и на практических примерахв "1С:Бухгалтерии государственного
                    учреждения 8", а также представлен обзор именений законодательства в 2014-2015 г.
                </blockquote>
                <h2>Настройка и техническая поддержка</h2>
                <div class="text__bq-clear">Поддержка пользователей осуществляется по телефону +7 (804) 333-80-14
                    и по технологии<br>1С-Коннект, а также по e-mail support. backup@1c. ru в режиме 24×7 без выходных и
                    праздничных дней. По вопросам, связанным с продуктами «1С:Облачныйархив», обращаться<br>по адресу
                    <a href="mailto:backup@1c.ru">backup@1c.ru</a>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="similar similar_mini visibility">
    <div class="similar__head">
        <div class="container container_narrow">
            <h2 class="similar__header">Другие наши продукты</h2>
            <div class="similar-slider__toolbar clearfix">
            </div>
        </div>
    </div>
    <div class="similar__body">
        <div class="similar__slider similar__slider_mini">
            <div class="similar-slide-wrap">
                <a href="#" class="link similar__slide"  title='1С:Предприятие 8. Управление корпоративными финансами'>
                    <span class="similar-slide__title">1С:Предприятие 8. Управление корпоративными финансами</span>
                </a>
            </div>
            <div class="similar-slide-wrap">
                <a href="#" class="link similar__slide"  title='1С:Предприятие 8. Комплект прикладных решений на 5 пользователей'>
                    <span class="similar-slide__title">1С:Предприятие 8. Комплект прикладных решений на 5 пользователей</span>
                </a>
            </div>
            <div class="similar-slide-wrap">
                <a href="#" class="link similar__slide"  title='1С:ERP Управление предприятием'>
                    <span class="similar-slide__title">1С:ERP Управление предприятием</span>
                </a>
            </div>
            <div class="similar-slide-wrap">
                <a href="#" class="link similar__slide"  title='1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"'>
                    <span class="similar-slide__title">1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"</span>
                </a>
            </div>
            <div class="similar-slide-wrap">
                <a href="#" class="link similar__slide"  title='"Ведение бюджетного учета в программе "1С:Бухгалтерия государственного учреждения'>
                    <span class="similar-slide__title">"Ведение бюджетного учета в программе "1С:Бухгалтерия государственного учреждения"Ведение бюджетного учета в программе "1С:Бухгалтерия государственного учреждения</span>
                </a>
            </div>
            <div class="similar-slide-wrap">
                <a href="#" class="link similar__slide"  title='1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"'>
                    <span class="similar-slide__title">1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"</span>
                </a>
            </div>
            <div class="similar-slide-wrap">
                <a href="#" class="link similar__slide"  title='1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"'>
                    <span class="similar-slide__title">1С:Предприятие 8. Использование конфигурации "Бухгалтерия предприятия"</span>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="consul">
    <div class="container container_narrow">
        <h1 class="consul__header">Необходима консультация?</h1>
        <div class="consul__desc">Бесплатно проконсультироваться, уточнить цены и заказать решение можно у специалистов
            нашей фирмы
        </div>
        <form action="#" class="consul__form feedback-form clearfix">
            <div class="feedback-form__group clearfix">
                <div class="consul__item">
                    <div class="feedback-form__group">
                        <input type="text" class="feedback-form__control feedback-form__control_type_text"
                               placeholder="Ваше имя">
                    </div>
                </div>
                <div class="consul__item">
                    <div class="feedback-form__group">
                        <input type="text" class="feedback-form__control feedback-form__control_type_text"
                               placeholder="Ваш телефон">
                    </div>
                </div>
                <div class="consul__item">
                    <div class="feedback-form__group">
                        <div class="feedback-form__control_type_select"><span
                                    class="feedback-form__control_type_current-option">Выберите город</span><span
                                    class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                        <ul class="feedback-form__control_type_options">
                            <li class="feedback-form__control_type_option">Город 1</li>
                            <li class="feedback-form__control_type_option">Город 2</li>
                            <li class="feedback-form__control_type_option">Город 3</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="feedback-form__group">
                <textarea
                        class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short"
                        placeholder="Ваш вопрос"></textarea>
            </div>
            <div class="feedback-form__group">
                <button class="btn btn_main feedback-form__submit"><span class="btn__link">Получить консультацию</span>
                </button>
                <div class="feedback-form__footnote"></div>
            </div>
            <div class="feedback-form__group clearfix">
                <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                    <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                    <span class="custom-control__indicator icon icon_checked"></span>
                    <span class="custom-control__name">Я согласен на обработку</span> <a href="#"
                                                                                         class="link custom-control__link"
                                                                                         title="Политика конфиденциальности">персональных
                        данных</a>
                </label>
            </div>
        </form>
    </div>
</section>
<footer class="footer">
    <div class="footer__consul-form feedback-form-modal-wrap" id="consul-form">
        <div class="feedback-form__title
            <h1 class=" feedback-form__title-valueНужна консультация?
        </h1>
        <div class="feedback-form__desc">Бесплатно проконсультироваться, уточнить цены и<br>заказать решение можно у
            специалистов нашей фирмы
        </div>
    </div>

    <form action="#" class="feedback-form feedback-form_modal">
        <div class="feedback-form__group">
            <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
        </div>
        <div class="feedback-form__group">
            <input type="text" class="feedback-form__control feedback-form__control_type_text"
                   placeholder="Ваш телефон">
        </div>
        <div class="feedback-form__group">
            <div class="feedback-form__control_type_select"><span class="feedback-form__control_type_current-option">Выберите город</span><span
                        class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
            <ul class="feedback-form__control_type_options">
                <li class="feedback-form__control_type_option">Город 1</li>
                <li class="feedback-form__control_type_option">Город 2</li>
                <li class="feedback-form__control_type_option">Город 3</li>
            </ul>
        </div>
        <div class="feedback-form__group">
            <textarea
                    class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short"
                    placeholder="Ваш вопрос"></textarea>
        </div>
        <div class="form__footer">
            <div class="feedback-form__group clearfix">
                <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                    <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                    <span class="custom-control__indicator icon icon_checked"></span>
                    <span class="custom-control__name">Я согласен на обработку</span> <a href="#"
                                                                                         class="link custom-control__link"
                                                                                         title="Политика конфиденциальности">персональных
                        данных</a>
                </label>
            </div>
            <div class="feedback-form__group">
                <button class="btn btn_main feedback-form__submit"><span class="btn__link">Получить консультацию</span>
                </button>
                <div class="feedback-form__footnote"></div>
            </div>
        </div>
    </form>

    </div>
    <div class="footer__feedback-form feedback-form-modal-wrap" id="feeedback-form">
        <div class="feedback-form__title
            <h1 class=" feedback-form__title-valueНаписать нам
        </h1>
    </div>
    <form action="#" class="feedback-form feedback-form_modal">
        <div class="feedback-form__group">
            <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
        </div>
        <div class="feedback-form__group">
            <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш email">
        </div>
        <div class="feedback-form__group">
            <textarea
                    class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short"
                    placeholder="Ваш вопрос"></textarea>
        </div>
        <div class="form__footer">
            <div class="feedback-form__group clearfix">
                <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                    <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                    <span class="custom-control__indicator icon icon_checked"></span>
                    <span class="custom-control__name">Я согласен на обработку</span> <a href="#"
                                                                                         class="link custom-control__link"
                                                                                         title="Политика конфиденциальности">персональных
                        данных</a>
                </label>
            </div>
            <div class="feedback-form__group">
                <button class="btn btn_main feedback-form__submit"><span class="btn__link">Отправить сообщение</span>
                </button>
                <div class="feedback-form__footnote"></div>
            </div>

        </div>
    </form>
    </div>
    <div class="footer__menu clearfix">
        <div class="container container_narrow">
            <div class="footer-menu__item">
                <div class="footer-menu__header">1С: Предприятие 8</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Бухгалтерия 8">1С: Бухгалтерия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Управление торговлей">1С: Управление торговлей</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Зарплата и Управление персоналом">1С: Зарплата и
                            Управление персоналом</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Комплексная автоматизация">1С: Комплексная
                            автоматизация</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Управление производственным предприятием">1С:
                            Управление<br>производственным предприятием</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Управление небольшой фирмой">1С: Управление
                            небольшой<br>фирмой</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <div class="footer-menu__header">Для бюджетников</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Бухгалтерия государственного учреждения">1С:
                            Бухгалтерия<br>государственного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Зарплата и кадры бюджетного учреждения">1С:
                            Зарплата и кадры<br>бюджетного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Бюджетная отчетность">1С: Бюджетная отчетность</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Свод отчетов">1С: Свод отчетов</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Государственные и муниципальные закупки">1С:
                            Государственные и муниципальные закупки</a></li>
                </ul>
            </div>
            <div class="footer-menu__item footer-menu__item_narrow">
                <div class="footer-menu__header">Услуги</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Консультации по выбору программного обеспечения">Консультации
                            по выбору программного обеспечения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Доставка и установка">Доставка и установка</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Настройка и внедрение">Настройка и внедрение</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Учебный центр">1С: Учебный центр</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Электронная отчетность из 1С: Предприятия 8">Электронная
                            отчетность из<br>1С: Предприятия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Центр сопровождение 1С">Центр сопровождение 1С</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <div class="footer-menu__header">О компании</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Статусы компании">Статусы компании</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Наши сотрудники">Наши сотрудники</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Вакансии">Вакансии</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Новости">Новости</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Фотогалерея">Фотогалерея</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Контакты">Контакты</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer__contact">
        <div class="container container_narrow">
            <div class="footer-contact__item">
                <div class="footer-contact__city">Пятигорск</div>
                <div class="footer-contact__tel">+7 (8793) 97-34-34</div>
                <div class="footer-contact__address">ул. Коста Хетагурова 4</div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Ставрополь</div>
                <div class="footer-contact__tel">+7 (8652) 301-103</div>
                <div class="footer-contact__address">ул. Мира, 360</div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Черкесск</div>
                <div class="footer-contact__tel">+7 (8782) 26-00-44</div>
                <div class="footer-contact__address">ул. Кирова, 21а (4-этаж)</div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact-btn-wrap">
                    <div class="btn btn_main footer-contact__btn"><a href="#consul-form"
                                                                     class="link btn__link footer-contact__btn-link footer__consul-opener"
                                                                     title="Получить консультацию">Получить
                            консультацию</a></div>
                    <div class="btn btn_linear footer-contact__btn"><a href="#feeedback-form"
                                                                       class="link btn__link footer-contact__btn-link footer__feedback-opener"
                                                                       title="Написать нам">Написать нам</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__signs">
        <div class="container container_narrow">
            <div class="footer__copyright">© 2002–2017. Все права защищены.</div>
            <div class="footer__dev-sign">
                <div class="footer-dev-sign__title">Создание сайта</div>
                <div class="footer-dev-sign__name">Студия Z-labs</div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="frontend/local/app.js"></script>
</body>
</html>