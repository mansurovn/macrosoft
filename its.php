<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="frontend/local/app.css">
    <title>ITS</title>
</head>
<body class="body">
<header class="header">
    <div class="container container_narrow">
        <div class="header__item">
            <a href="#" class="header__brand header__logo" title="Главная"></a>
        </div>
        <div class="header__item">
            <div class="header__slogan">Простые решения<br>для вашего бизнеса</div>
        </div>
        <div class="header__item">
            <div class="header__location-and-tel">
                <div class="header__location">
                    <button class="header-location__btn header__title"><span class="header-location__showcase"><span
                                    class="header-location__link icon icon_arrow-angle-down">Пятигорск</span></span>
                    </button>
                    <div class="header-location__list">
                        <div class="header-location__item header-location__item_active">Пятигорск</div>
                        <div class="header-location__item">Ставрополь</div>
                        <div class="header-location__item">Черкесск</div>
                    </div>
                </div>
                <div class="header__text header__tel">+7 (8793) 97-34-34</div>
            </div>
        </div>
        <div class="header__item">
            <div class="header__mail">
                <div class="header__title">Эл. почта</div>
                <div class="header__text">info@mskmv.ru</div>
            </div>
        </div>
        <div class="header__item">
            <a href="#" class="header__brand header__1c" title="1C:Центр Сопровождения"></a>
        </div>
    </div>
</header>
<nav class="nav">
    <div class="container container_narrow">
        <ul class="nav__list clearfix">
            <li class="nav__item">
                <a href="#" class="link nav__link" title="О нас"><span class="nav__undescore">О нас</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Продукты"><span class="nav__undescore">Продукты</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Услуги"><span class="nav__undescore">Услуги</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Сопровождение"><span class="nav__undescore">1С:Сопровождение</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Учебный центр"><span class="nav__undescore">1С:Учебный центр</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Отчетность"><span
                            class="nav__undescore">1С:Отчетность</span></a>
            </li>
        </ul>
    </div>
</nav>
<div class="breadcrumbs">
    <div class="container container_narrow">
        <a href="#" class="link breadcrumbs__link icon icon_arrow-right" title="Главная">Главная</a>
        <a href="#" class="link breadcrumbs__link icon icon_arrow-right" title="Услуги">Услуги</a>
        <span class="breadcrumbs__dest">Информационно-технологическое сопровождение (ИТС)</span>
    </div>
</div>
<div class="main clearfix">
    <div class="container container_narrow">
        <h1 class="h1">Информационно-технологическое<br>сопровождение (ИТС)</h1>
        <div class="content">
            <div class="its text">
                <p>Многолетний опыт работы нашей компании позволил создать уникальную систему сервиса и сопровождения
                    пользователей программ 1С:Предприятие.</p>
                <p>Уникальность технологии сопровождения заключается в наличии разработанного стандарта качества,
                    который прошел сертификацию на соответствие всем требованиям фирмы "1С", что подтверждается высоким
                    статусом Центра сопровождения фирмы "1С" и соответствующим сертификатом. Кроме того, стандарт
                    качества сопровождения нашей компании прошел сертификацию на соответствие уровня международного
                    стандарта качества, что так же подтверждено соответствующими международными сертификатами. Но самым
                    главным подтверждением нашей квалификации является доверие наших клиентов и отзывы, благодаря
                    которым мы постоянно совершенствуем свою работу. Ежемесячный рост клиентов компании подтверждает
                    высокую востребованность созданной нами технологии. За время работы более 10000 компаний региона
                    оценили все преимущества работы с нами.</p>
                <div class="rate rate-ref" id="rates">
                    <h2 class="rate__header">Тарифы сопровождения</h2>
                    <div class="rate__panels clearfix visibility">
                        <div class="rate__panel">
                            <div class="rate-panel__subheader">Тариф</div>
                            <div class="rate-panel__header">«Базовый»</div>
                            <div class="rate-panel__duration">
                                <span class="rate-panel-duration__text">При оплате за </span>
                                <button class="rate-panel__variant pop-up__trigger icon icon_arrow-angle-down">
                                <span class="rate-panel-variant__text">
                                    <span class="rate-panel-variant__current pop-up-list__current">12 месяцев</span>
                                    <span class="pop-up pop-up-list-wrap pop-up_under">
                                        <span class="pop-up__list">
                                            <span class="pop-up-list__item">1 месяц</span>
                                            <span class="pop-up-list__item">3 месяца</span>
                                            <span class="pop-up-list__item">6 месяцев</span>
                                            <span class="pop-up-list__item pop-up-list__item_active">12 месяцев</span>
                                            <span class="pop-up-list__item">24 месяца</span>
                                        </span>
                                    </span>
                                </span></button>
                            </div>
                            <div class="rate-panel__prices">
                                <div class="rate-panel__price">
                                    <div class="rate-panel-price__text">
                                        Льготная цена
                                        <span class="rate-panel-price__about">
                                        ?
                                        <span class="pop-up pop-up_black pop-up_above rate__pop-up">Льготная цена доступна только при непрерывном продлении договора </span>
                                    </span></div>
                                    <div class="rate-panel-price__number">12 156 <span
                                                class="rate-panel-price__currency">"</span></div>
                                </div>
                                <hr>
                                <div class="rate-panel__price">
                                    <div class="rate-panel-price__text">
                                        Возобновляемая цена
                                        <span class="rate-panel-price__about">
                                        ?
                                        <span class="pop-up pop-up_black pop-up_above rate__pop-up">Льготная цена доступна только при непрерывном продлении договора </span>
                                    </span></div>
                                    <div class="rate-panel-price__number">16 264 <span
                                                class="rate-panel-price__currency">"</span></div>
                                </div>
                            </div>
                            <div class="btn btn_linear rate-panel__btn">
                                <a href="#" class="link btn btn__link rate-panel__btn-link" title="Оставить заявку">Оставить
                                    заявку</a>
                            </div>
                            <a href="#rate-tabs__tabpanel1" class="link rate-panel-about__link rate-ref__link"
                               title="Подробности" role="tab" data-toggle="tab">Подробности</a>
                        </div>
                        <div class="rate__panel">
                            <div class="rate-panel__subheader">Тариф</div>
                            <div class="rate-panel__header">«Оптимальный»</div>
                            <div class="rate-panel__duration">
                                <span class="rate-panel-duration__text">При оплате за </span>
                                <button class="rate-panel__variant pop-up__trigger icon icon_arrow-angle-down">
                                <span class="rate-panel-variant__text">
                                    <span class="rate-panel-variant__current pop-up-list__current ">12 месяцев</span>
                                    <span class="pop-up pop-up-list-wrap pop-up_under">
                                        <span class="pop-up__list">
                                            <span class="pop-up-list__item">1 месяц</span>
                                            <span class="pop-up-list__item">3 месяца</span>
                                            <span class="pop-up-list__item">6 месяцев</span>
                                            <span class="pop-up-list__item pop-up-list__item_active">12 месяцев</span>
                                            <span class="pop-up-list__item">24 месяца</span>
                                        </span>
                                    </span>
                                </span>
                                </button>
                            </div>
                            <div class="rate-panel__prices">
                                <div class="rate-panel__price">
                                    <div class="rate-panel-price__text">
                                        Льготная цена
                                        <span class="rate-panel-price__about">
                                        ?
                                        <span class="pop-up pop-up_black pop-up_above rate__pop-up">Льготная цена доступна только при непрерывном продлении договора </span>
                                    </span></div>
                                    <div class="rate-panel-price__number">29 664 <span
                                                class="rate-panel-price__currency">"</span></div>
                                </div>
                                <hr>
                                <div class="rate-panel__price">
                                    <div class="rate-panel-price__text">
                                        Возобновляемая цена
                                        <span class="rate-panel-price__about">
                                        ?
                                        <span class="pop-up pop-up_black pop-up_above rate__pop-up">Льготная цена доступна только при непрерывном продлении договора </span>
                                    </span></div>
                                    <div class="rate-panel-price__number">35 592 <span
                                                class="rate-panel-price__currency">"</span></div>
                                </div>
                            </div>
                            <div class="btn btn_linear rate-panel__btn">
                                <a href="#" class="link btn btn__link rate-panel__btn-link" title="Оставить заявку">Оставить
                                    заявку</a>
                            </div>
                            <a href="#rate-tabs__tabpanel2" class="link rate-panel-about__link rate-ref__link"
                               title="Подробности" role="tab" data-toggle="tab">Подробности</a>
                        </div>
                        <div class="rate__panel">
                            <div class="rate-panel__subheader">Тариф</div>
                            <div class="rate-panel__header">«Расширенный»</div>
                            <div class="rate-panel__duration">
                                <span class="rate-panel-duration__text">При оплате за </span>
                                <button class="rate-panel__variant pop-up__trigger icon icon_arrow-angle-down">
                                <span class="rate-panel-variant__text">
                                    <span class="rate-panel-variant__current pop-up-list__current">24 месяца</span>
                                    <span class="pop-up pop-up-list-wrap pop-up_under">
                                        <span class="pop-up__list">
                                            <span class="pop-up-list__item">1 месяц</span>
                                            <span class="pop-up-list__item">3 месяца</span>
                                            <span class="pop-up-list__item">6 месяцев</span>
                                            <span class="pop-up-list__item">12 месяцев</span>
                                            <span class="pop-up-list__item pop-up-list__item_active">24 месяца</span>
                                        </span>
                                    </span>
                                </span></button>
                            </div>
                            <div class="rate-panel__prices">
                                <div class="rate-panel__price">
                                    <div class="rate-panel-price__text">
                                        Льготная цена
                                        <span class="rate-panel-price__about">
                                        ?
                                        <span class="pop-up pop-up_black pop-up_above rate__pop-up">Льготная цена доступна только при непрерывном продлении договора </span>
                                    </span></div>
                                    <div class="rate-panel-price__number">42 659 <span
                                                class="rate-panel-price__currency">"</span></div>
                                </div>
                            </div>
                            <div class="btn btn_linear rate-panel__btn">
                                <a href="#" class="link btn btn__link rate-panel__btn-link" title="Оставить заявку">Оставить
                                    заявку</a>
                            </div>
                            <a href="#rate-tabs__tabpanel3" class="link rate-panel-about__link rate-ref__link"
                               role="tab" data-toggle="tab"
                               title="Подробности">Подробности</a>
                        </div>
                    </div>
                    <div class="rate-tabs rate-ref__anchor">
                        <div class="rate-tabs__tablist" role="tablist">
                            <div class="rate-tabs-tab-wrap">
                                <a href="#rate-tabs__tabpanel1" class="link rate-tabs__tab" title="Базовый" role="tab"
                                   data-toggle="tab">Базовый</a>
                            </div>
                            <div class="rate-tabs-tab-wrap">
                                <a href="#rate-tabs__tabpanel2" class="link rate-tabs__tab" title="Оптимальный"
                                   role="tab" data-toggle="tab">Оптимальный</a>
                            </div>
                            <div class="rate-tabs-tab-wrap">
                                <a href="#rate-tabs__tabpanel3" class="link rate-tabs__tab" title="Расширенный"
                                   role="tab" data-toggle="tab">Расширенный</a>
                            </div>
                        </div>
                        <div class="rate-tabs__tabpanels">
                            <div class="rate-tabs__tabpanel tab-panel" role="tabpanel" id="rate-tabs__tabpanel1">
                                <table class="rate-tab__table">
                                    <tbody>
                                    <tr>
                                        <th>Вид работ</th>
                                        <th>Условия предоставления</th>
                                        <th>Периодичность</th>
                                    </tr>
                                    <tr>
                                        <td>Обновление типовых конфигураций посредствомнастройки автоматического
                                            обновления
                                        </td>
                                        <td>Не более трех информационных баз</td>
                                        <td>В течении срока действия договора</td>
                                    </tr>
                                    <tr>
                                        <td>Обновление программных файлов платформы 1С:Предприятие по средством
                                            удаленного подключенияили с выездом специалиста
                                        </td>
                                        <td>Обновление выполняется для одного ПК</td>
                                        <td>По мере технических требований используемых конфигураций 1С</td>
                                    </tr>
                                    <tr>
                                        <td>Доставка комплекта поставки (DVD-выпуск, текущий выпускжурнала "БУХ.1С",
                                            сувенир делового назначения)
                                        </td>
                                        <td>Сервис-инженер или экспедитор или почта России</td>
                                        <td>1 раз в месяц</td>
                                    </tr>
                                    <tr>
                                        <td>Линия консультаций фирмы «1С»</td>
                                        <td>с 10-00 до 18-00 по рабочим дням, e-mail:v8@1c.ru, по телефону (495)
                                            956-11-81
                                        </td>
                                        <td>1 раз в месяц</td>
                                    </tr>
                                    <tr>
                                        <td>Обновление типовых конфигураций посредствомнастройки автоматического
                                            обновления
                                        </td>
                                        <td>Не более трех информационных баз</td>
                                        <td>В течении срока действия договора</td>
                                    </tr>
                                    <tr>
                                        <td>Обновление программных файлов платформы 1С:Предприятие по средством
                                            удаленного подключенияили с выездом специалиста
                                        </td>
                                        <td>Обновление выполняется для одного ПК</td>
                                        <td>По мере технических требований используемых конфигураций 1С</td>
                                    </tr>
                                    <tr>
                                        <td>Доставка комплекта поставки (DVD-выпуск, текущий выпускжурнала "БУХ.1С",
                                            сувенир делового назначения)
                                        </td>
                                        <td>Сервис-инженер или экспедитор или почта России</td>
                                        <td>1 раз в месяц</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="rate-tabs__tabpanel tab-panel" role="tabpanel" id="rate-tabs__tabpanel2">
                                <table class="rate-tab__table">
                                    <tbody>
                                    <tr>
                                        <th>Вид работ</th>
                                        <th>Условия предоставления</th>
                                        <th>Периодичность</th>
                                    </tr>
                                    <tr>
                                        <td>Обновление типовых конфигураций посредствомнастройки автоматического
                                            обновления
                                        </td>
                                        <td>Не более трех информационных баз</td>
                                        <td>В течении срока действия договора</td>
                                    </tr>
                                    <tr>
                                        <td>Обновление программных файлов платформы 1С:Предприятие по средством
                                            удаленного подключенияили с выездом специалиста
                                        </td>
                                        <td>Обновление выполняется для одного ПК</td>
                                        <td>По мере технических требований используемых конфигураций 1С</td>
                                    </tr>
                                    <tr>
                                        <td>Доставка комплекта поставки (DVD-выпуск, текущий выпускжурнала "БУХ.1С",
                                            сувенир делового назначения)
                                        </td>
                                        <td>Сервис-инженер или экспедитор или почта России</td>
                                        <td>1 раз в месяц</td>
                                    </tr>
                                    <tr>
                                        <td>Линия консультаций фирмы «1С»</td>
                                        <td>с 10-00 до 18-00 по рабочим дням, e-mail:v8@1c.ru, по телефону (495)
                                            956-11-81
                                        </td>
                                        <td>1 раз в месяц</td>
                                    </tr>
                                    <tr>
                                        <td>Обновление типовых конфигураций посредствомнастройки автоматического
                                            обновления
                                        </td>
                                        <td>Не более трех информационных баз</td>
                                        <td>В течении срока действия договора</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="rate-tabs__tabpanel tab-panel" role="tabpanel" id="rate-tabs__tabpanel3">
                                <table class="rate-tab__table">
                                    <tbody>
                                    <tr>
                                        <th>Вид работ</th>
                                        <th>Условия предоставления</th>
                                        <th>Периодичность</th>
                                    </tr>
                                    <tr>
                                        <td>Обновление типовых конфигураций посредствомнастройки автоматического
                                            обновления
                                        </td>
                                        <td>Не более трех информационных баз</td>
                                        <td>В течении срока действия договора</td>
                                    </tr>
                                    <tr>
                                        <td>Обновление программных файлов платформы 1С:Предприятие по средством
                                            удаленного подключенияили с выездом специалиста
                                        </td>
                                        <td>Обновление выполняется для одного ПК</td>
                                        <td>По мере технических требований используемых конфигураций 1С</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="rate-btn-wrap">
                            <button class="btn btn_trigger rate__btn rate-ref__hider">Скрыть подробности</button>
                            <!--<div class="btn btn_main rate__btn"><a href="#" class="link btn__link rate__btn-link" role="tab" data-toggle="tab">Подобрать тариф</a></div>-->
                        </div>
                    </div>
                </div>
                <div class="special-rate">
                    <div class="special-rate-toggle-wrap">
                        <button class="link special-rate__toggle">Скрыть специализированные тарифы</button>
                    </div>
                    <div class="special-rate__content">
                        <div class="special-rate__tabs" role="tablist">
                            <div class="special-rate-tab-wrap">
                                <a href="#special-rate__tabpanel1"
                                   class="link special-rate__tab special-rate__tab_active" title="ИТС строительство"
                                   role="tab" data-toggle="tab">ИТС строительство</a>
                            </div>
                            <div class="special-rate-tab-wrap">
                                <a href="#special-rate__tabpanel2" class="link special-rate__tab" title="ИТС медицина"
                                   role="tab" data-toggle="tab">ИТС медицина</a>
                            </div>
                            <div class="special-rate-tab-wrap">
                                <a href="#special-rate__tabpanel3" class="link special-rate__tab"
                                   title="ИТС бюджет проф"
                                   role="tab" data-toggle="tab">ИТС бюджет проф</a>
                            </div>
                        </div>
                        <div class="special-rate__tabpanels">
                            <div class="special-rate__tabpanel tab-panel tab-panel_active tab-panel_in" role="tabpanel"
                                 id="special-rate__tabpanel1">
                                <div class="special-rate__panels clearfix">
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">3 месяца</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">16 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена  13 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">3 месяца</div>
                                                <div class="special-rate__sale">(спецпредложение 8+4)</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена  813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">3 месяца</div>
                                                <div class="special-rate__sale">(спецпредложение 8+4) (спецпредложение
                                                    8+4)
                                                </div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">3 816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС При продлении ИТС При
                                            продлении ИТС
                                        </div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 2 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">43 816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)* (скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">43 816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)* (скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)* (скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">16 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="special-rate__tables clearfix">
                                    <div class="special-rate-table__container">
                                        <h2 class="h2 special-rate___header">Опции тарифа</h2>
                                        <table class="special-rate__table special-rate__table_main">
                                            <tbody>
                                            <tr>
                                                <td>Выезд специалиста</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr class="disabled">
                                                <td>Обновление 1 информационной базы</td>
                                                <td class="icon icon_dash_thick"></td>
                                            </tr>
                                            <tr>
                                                <td>Обновление 3 информационных баз</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Журнал «Бух.1C»</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Доступ к обновлениям на сайте</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Доступ к интернет-версии на сайте</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Создание архивных копий баз данных</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Диагностика баз данных</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Бесплатная линия консультаций</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Отправка обновлений по запросу на электронную почту</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Приоритетное рассмотрение сообщений на Линии Консультации 1С</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Доступ к ответам на вопросы технической поддержки фирмы 1С</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Консультации аудиторов</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Методические материалы</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="special-rate-table__container">
                                        <h2 class="h2 special-rate___header">Дополнительные опции</h2>
                                        <table class="special-rate__table special-rate__table_add">
                                            <tbody>
                                            <tr>
                                                <td>Дополнительный час работ</td>
                                                <td>2 000 <span class="special-rate-table__currency">й</span></td>
                                            </tr>
                                            <tr>
                                                <td>Обновление одной дополнительной информационной базы</td>
                                                <td>500 <span class="special-rate-table__currency">й</span>/мес.</td>
                                            </tr>
                                            <tr>
                                                <td>Срочный выезд</td>
                                                <td>300 <span class="special-rate-table__currency">й</span>/мес.</td>
                                            </tr>
                                            <tr>
                                                <td>Доставка DVD выпуска 1С:ИТС почтой</td>
                                                <td>300 <span class="special-rate-table__currency">й</span>/мес.</td>
                                            </tr>
                                            <tr>
                                                <td>Выезд за пределы МКАД до 10 км</td>
                                                <td>750 <span class="special-rate-table__currency">й</span></td>
                                            </tr>
                                            <tr>
                                                <td>Выезд за пределы МКАД до 11–20 км</td>
                                                <td>1000 <span class="special-rate-table__currency">й</span></td>
                                            </tr>
                                            <tr>
                                                <td>Выезд за пределы МКАД до 21–50 км</td>
                                                <td>1350 <span class="special-rate-table__currency">й</span></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="special-rate__tabpanel tab-panel" role="tabpanel" id="special-rate__tabpanel2">
                                <div class="special-rate__panels clearfix">
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">3 месяца</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">16 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена  13 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">3 месяца</div>
                                                <div class="special-rate__sale">(спецпредложение 8+4)</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена  813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">3 месяца</div>
                                                <div class="special-rate__sale">(спецпредложение 8+4) (спецпредложение
                                                    8+4)
                                                </div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">3 816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС При продлении ИТС При
                                            продлении ИТС
                                        </div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 2 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">43 816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)* (скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">43 816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="special-rate__tables clearfix">
                                    <div class="special-rate-table__container">
                                        <h2 class="h2 special-rate___header">Опции тарифа</h2>
                                        <table class="special-rate__table special-rate__table_main">
                                            <tbody>
                                            <tr>
                                                <td>Выезд специалиста</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr class="disabled">
                                                <td>Обновление 1 информационной базы</td>
                                                <td class="icon icon_dash_thick"></td>
                                            </tr>
                                            <tr>
                                                <td>Обновление 3 информационных баз</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Журнал «Бух.1C»</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Доступ к обновлениям на сайте</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Доступ к интернет-версии на сайте</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Создание архивных копий баз данных</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Диагностика баз данных</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Бесплатная линия консультаций</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Отправка обновлений по запросу на электронную почту</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Приоритетное рассмотрение сообщений на Линии Консультации 1С</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Доступ к ответам на вопросы технической поддержки фирмы 1С</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Консультации аудиторов</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            <tr>
                                                <td>Методические материалы</td>
                                                <td class="icon icon_checked2"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="special-rate-table__container">
                                        <h2 class="h2 special-rate___header">Дополнительные опции</h2>
                                        <table class="special-rate__table special-rate__table_add">
                                            <tbody>
                                            <tr>
                                                <td>Дополнительный час работ</td>
                                                <td>2 000 <span class="special-rate-table__currency">й</span></td>
                                            </tr>
                                            <tr>
                                                <td>Обновление одной дополнительной информационной базы</td>
                                                <td>500 <span class="special-rate-table__currency">й</span>/мес.</td>
                                            </tr>
                                            <tr>
                                                <td>Срочный выезд</td>
                                                <td>300 <span class="special-rate-table__currency">й</span>/мес.</td>
                                            </tr>
                                            <tr>
                                                <td>Доставка DVD выпуска 1С:ИТС почтой</td>
                                                <td>300 <span class="special-rate-table__currency">й</span>/мес.</td>
                                            </tr>
                                            <tr>
                                                <td>Выезд за пределы МКАД до 10 км</td>
                                                <td>750 <span class="special-rate-table__currency">й</span></td>
                                            </tr>
                                            <tr>
                                                <td>Выезд за пределы МКАД до 11–20 км</td>
                                                <td>1000 <span class="special-rate-table__currency">й</span></td>
                                            </tr>
                                            <tr>
                                                <td>Выезд за пределы МКАД до 21–50 км</td>
                                                <td>1350 <span class="special-rate-table__currency">й</span></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="special-rate__tabpanel tab-panel" role="tabpanel" id="special-rate__tabpanel3">
                                <div class="special-rate__panels clearfix">
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">3 месяца</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">16 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена  13 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">3 месяца</div>
                                                <div class="special-rate__sale">(спецпредложение 8+4)</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена  813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">3 месяца</div>
                                                <div class="special-rate__sale">(спецпредложение 8+4) (спецпредложение
                                                    8+4)
                                                </div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">3 816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС При продлении ИТС При
                                            продлении ИТС
                                        </div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 2 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">43 816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)* (скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">43 816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)* (скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">816 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)* (скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">16 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)* (скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">16 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)* (скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">16 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)* (скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">16 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)* (скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">16 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                    <div class="special-rate__panel">
                                        <div class="special-rate-duration-and-sale-wrap">
                                            <div class="special-rate__duration-and-sale">
                                                <div class="special-rate__duration">24 месяца</div>
                                                <div class="special-rate__sale">(скидка 10%)* (скидка 10%)*</div>
                                            </div>
                                        </div>
                                        <div class="special-rate-price__container">
                                            <div class="special-rate__price"><span
                                                        class="special-rate-price__value">16 264 </span><span
                                                        class="special-rate-price__currency">"</span></div>
                                        </div>
                                        <div class="special-rate__condition">При продлении ИТС</div>
                                        <div class="btn btn_linear special-rate-panel__btn">
                                            <a href="#" class="link btn__link special-rate-panel__btn-link"
                                               title="Оставить заявку">Оставить заявку</a>
                                        </div>
                                        <div class="special-rate__recommended"><span
                                                    class="special-rate-recommended__value">Рекомендованная цена 22 813 233 </span><span
                                                    class="special-rate-recommended__currency">"</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="special-rate__footer">
                            <div class="btn btn_linear special-rate__btn special-rate-footer__toggle">
                                <a href="#question" class="link btn__link special-rate__btn-link" title="Задать вопрос">Задать
                                    вопрос</a>
                            </div>
                            <div class="btn btn_main special-rate__btn">
                                <a href="#" class="link btn__link special-rate__btn-link" title="Подобрать тариф">Подобрать
                                    тариф</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="attendance-section visibility">
    <div class="container container_narrow">
        <h2 class="h2 attendance-section__header">Сервисы</h2>
        <div class="attendance__list clearfix">
            <div class="attendance__item">
                <a href="#" class="attendance-item__link" title="1С-ЭДО"></a>
                <div class="attendance-item__icon-box">
                    <div class="attendance-item__icon sprite sprite_open_book"></div>
                </div>
                <div class="attendance-item__content">

                    <div class="attendance-item__title">1С-ЭДО</div>
                    <div class="attendance-item__text">Электронный документооборот на любом расстоянии за 5 мин.
                    </div>
                    <div class="attendance-item__cats">
                        <span class="attendance-item-cats__title">В тарифах:  </span><a href="/its.php#rates"
                                                                                        class="link attendance-item-cats__cat" title="оптимальный">оптимальный</a>, <a href="/its.php#rates"
                                                                                                                                                                       class="link attendance-item-cats__cat" title="расширенный">расширенный</a>
                    </div>
                </div>
            </div>
            <div class="attendance__item">
                <a href="#" class="attendance-item__link" title="1С-ЭДО"></a>
                <div class="attendance-item__icon-box">
                    <div class="attendance-item__icon sprite sprite_open_book"></div>
                </div>
                <div class="attendance-item__content">

                    <div class="attendance-item__title">1С-Отчетность</div>
                    <div class="attendance-item__text">Сдать НДС и другие отчеты в электронном виде.</div>
                </div>
            </div>
            <div class="attendance__item">
                <a href="#" class="attendance-item__link" title="1С:ИТС Отраслевой"></a>
                <div class="attendance-item__icon-box">
                    <div class="attendance-item__icon sprite sprite_open_book"></div>
                </div>
                <div class="attendance-item__content">

                    <div class="attendance-item__title">1С:ИТС Отраслевой</div>
                    <div class="attendance-item__text">Обновления 1С, базы знаний 1С, линия консультаций ра...</div>
                    <div class="attendance-item__cats">
                        <span class="attendance-item-cats__title">В тарифах:  </span><a href="/its.php#rates"
                                                                                        class="link attendance-item-cats__cat" title="оптимальный">оптимальный</a>, <a href="/its.php#rates"
                                                                                                                                                                       class="link attendance-item-cats__cat" title="расширенный">расширенный</a>
                    </div>
                </div>
            </div>
            <div class="attendance__item">
                <a href="#" class="attendance-item__link" title="1С:ИТС Отраслевой"></a>
                <div class="attendance-item__icon-box">
                    <div class="attendance-item__icon sprite sprite_open_book"></div>
                </div>
                <div class="attendance-item__content">

                    <div class="attendance-item__title">1С:Облачный архив</div>
                    <div class="attendance-item__text">Безопасное хранение и размещение копии ваших данных.</div>
                    <div class="attendance-item__cats">
                        <span class="attendance-item-cats__title">В тарифах:  </span><a href="/its.php#rates"
                                                                                        class="link attendance-item-cats__cat" title="оптимальный">оптимальный</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="attendance-section__footer">
            <div class="infinity attendance-section-refresh">
                <a href="/its.php" class="link attendance-section__refresh-link attendance-section__refresh-icon icon icon_rotate" data-load="Загрузка..." id="infinity-next-page"><span class="infinity__link-text attendance-section__refresh-underline">Еще сервисы</span></a>
            </div>
        </div>
    </div>
</section>
<section class="review review_short visibility">
    <div class="container container_narrow">
        <div class="review__head">
            <h1 class="h1 review__header">Отзывы клиентов</h1>
        </div>
        <div class="review__body">
            <div class="review__slider">
                <div class="review-slide-wrap">
                    <div class="review__slide">
                        <div class="review-slide__img"></div>
                        <div class="review-slide__title-and-text">
                            <div class="review-slide__title">Вирта Татьяна Александровна</div>
                            <div class="review-slide__text">Статус Центра Сертифицированного Обучения позволяет
                                Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                курсам, разработанным фирмой «1С». Статус Центра Сертифицированного Обучения позволяет
                                Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                курсам, разработанным фирмой «1С».
                                Статус Центра Сертифицированного Обучения позволяет
                                Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                курсам, разработанным фирмой «1С».
                            </div>
                        </div>
                    </div>
                </div>
                <div class="review-slide-wrap">
                    <div class="review__slide">
                        <div class="review-slide__img"></div>
                        <div class="review-slide__title-and-text">
                            <div class="review-slide__title">Вирта Татьяна Александровна</div>
                            <div class="review-slide__text">Статус Центра Сертифицированного Обучения позволяет
                                Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                курсам, разработанным фирмой «1С».Статус Центра Сертифицированного Обучения позволяет
                                Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                курсам, разработанным фирмой «1С».Статус Центра Сертифицированного Обучения позволяет
                                Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                курсам, разработанным фирмой «1С».Статус Центра Сертифицированного Обучения позволяет
                                Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                курсам, разработанным фирмой «1С».
                                Статус Центра Сертифицированного Обучения позволяет
                                Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                курсам, разработанным фирмой «1С».
                                Статус Центра Сертифицированного Обучения позволяет
                                Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                курсам, разработанным фирмой «1С».
                            </div>
                        </div>
                    </div>
                </div>
                <div class="review-slide-wrap">
                    <div class="review__slide">
                        <div class="review-slide__img"></div>
                        <div class="review-slide__title-and-text">
                            <div class="review-slide__title">Вирта Татьяна Александровна</div>
                            <div class="review-slide__text">Статус Центра Сертифицированного Обучения позволяет
                                Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                курсам, разработанным фирмой «1С».
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="review__footer">
            <button class="btn btn_linear review__btn review__expander"><span
                        class="link btn__link review__btn-link">Читать отзыв целиком</span></button>
            <div class="btn btn_main review__btn review__btn_main"><a href="#review__form"
                                                                      class="link btn__link review__btn-link review__pop-up-opener">Оставить
                    свой отзыв</a></div>
        </div>
    </div>
    <div class="review__form feedback-form-modal-wrap" id="review__form">
        <div class="feedback-form__title">
            <h1 class="feedback-form__title-value">Добавить отзыв</h1>
        </div>
        <form action="#" class="feedback-form feedback-fom_modal">
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text"
                       placeholder="Введите ФИО">
            </div>
            <div class="feedback-form__group">
                    <textarea
                            class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_tall"
                            placeholder="Текст отзыва"></textarea>
            </div>
            <div class="feedback-form__footer">
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#"
                                                                                             class="link custom-control__link"
                                                                                             title="Политика конфиденциальности">персональных
                            данных</a>
                    </label>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span class="btn__link">Добавить отзыв</span>
                    </button>
                    <div class="feedback-form__footnote"></div>
                </div>
            </div>
        </form>
    </div>
</section>
<section class="consul">
    <div class="container container_narrow">
        <h1 class="consul__header">Необходима консультация?</h1>
        <div class="consul__desc">Бесплатно проконсультироваться, уточнить цены и заказать решение можно у специалистов
            нашей фирмы
        </div>
        <form action="#" class="consul__form feedback-form clearfix">
            <div class="feedback-form__group clearfix">
                <div class="consul__item">
                    <div class="feedback-form__group">
                        <input type="text" class="feedback-form__control feedback-form__control_type_text"
                               placeholder="Ваше имя">
                    </div>
                </div>
                <div class="consul__item">
                    <div class="feedback-form__group">
                        <input type="text" class="feedback-form__control feedback-form__control_type_text"
                               placeholder="Ваш телефон">
                    </div>
                </div>
                <div class="consul__item">
                    <div class="feedback-form__group">
                        <div class="feedback-form__control_type_select"><span
                                    class="feedback-form__control_type_current-option">Выберите город</span><span
                                    class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                        <ul class="feedback-form__control_type_options">
                            <li class="feedback-form__control_type_option">Город 1</li>
                            <li class="feedback-form__control_type_option">Город 2</li>
                            <li class="feedback-form__control_type_option">Город 3</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="feedback-form__group">
                <textarea
                        class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short"
                        placeholder="Ваш вопрос"></textarea>
            </div>
            <div class="feedback-form__group">
                <button class="btn btn_main feedback-form__submit"><span class="btn__link">Получить консультацию</span>
                </button>
                <div class="feedback-form__footnote"></div>
            </div>
            <div class="feedback-form__group clearfix">
                <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                    <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                    <span class="custom-control__indicator icon icon_checked"></span>
                    <span class="custom-control__name">Я согласен на обработку</span> <a href="#"
                                                                                         class="link custom-control__link"
                                                                                         title="Политика конфиденциальности">персональных
                        данных</a>
                </label>
            </div>
        </form>
    </div>
</section>
<footer class="footer">
    <div class="footer__consul-form feedback-form-modal-wrap" id="consul-form">
        <div class="feedback-form__title
            <h1 class=" feedback-form__title-valueНужна консультация?
        </h1>
        <div class="feedback-form__desc">Бесплатно проконсультироваться, уточнить цены и<br>заказать решение можно у
            специалистов нашей фирмы
        </div>
    </div>

    <form action="#" class="feedback-form feedback-form_modal">
        <div class="feedback-form__group">
            <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
        </div>
        <div class="feedback-form__group">
            <input type="text" class="feedback-form__control feedback-form__control_type_text"
                   placeholder="Ваш телефон">
        </div>
        <div class="feedback-form__group">
            <div class="feedback-form__control_type_select"><span class="feedback-form__control_type_current-option">Выберите город</span><span
                        class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
            <ul class="feedback-form__control_type_options">
                <li class="feedback-form__control_type_option">Город 1</li>
                <li class="feedback-form__control_type_option">Город 2</li>
                <li class="feedback-form__control_type_option">Город 3</li>
            </ul>
        </div>
        <div class="feedback-form__group">
            <textarea
                    class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short"
                    placeholder="Ваш вопрос"></textarea>
        </div>
        <div class="form__footer">
            <div class="feedback-form__group clearfix">
                <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                    <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                    <span class="custom-control__indicator icon icon_checked"></span>
                    <span class="custom-control__name">Я согласен на обработку</span> <a href="#"
                                                                                         class="link custom-control__link"
                                                                                         title="Политика конфиденциальности">персональных
                        данных</a>
                </label>
            </div>
            <div class="feedback-form__group">
                <button class="btn btn_main feedback-form__submit"><span class="btn__link">Получить консультацию</span>
                </button>
                <div class="feedback-form__footnote"></div>
            </div>
        </div>
    </form>

    </div>
    <div class="footer__feedback-form feedback-form-modal-wrap" id="feeedback-form">
        <div class="feedback-form__title
            <h1 class=" feedback-form__title-valueНаписать нам
        </h1>
    </div>
    <form action="#" class="feedback-form feedback-form_modal">
        <div class="feedback-form__group">
            <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
        </div>
        <div class="feedback-form__group">
            <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш email">
        </div>
        <div class="feedback-form__group">
            <textarea
                    class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short"
                    placeholder="Ваш вопрос"></textarea>
        </div>
        <div class="form__footer">
            <div class="feedback-form__group clearfix">
                <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                    <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                    <span class="custom-control__indicator icon icon_checked"></span>
                    <span class="custom-control__name">Я согласен на обработку</span> <a href="#"
                                                                                         class="link custom-control__link"
                                                                                         title="Политика конфиденциальности">персональных
                        данных</a>
                </label>
            </div>
            <div class="feedback-form__group">
                <button class="btn btn_main feedback-form__submit"><span class="btn__link">Отправить сообщение</span>
                </button>
                <div class="feedback-form__footnote"></div>
            </div>

        </div>
    </form>
    </div>
    <div class="footer__menu clearfix">
        <div class="container container_narrow">
            <div class="footer-menu__item">
                <div class="footer-menu__header">1С: Предприятие 8</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Бухгалтерия 8">1С: Бухгалтерия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Управление торговлей">1С: Управление торговлей</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Зарплата и Управление персоналом">1С: Зарплата и
                            Управление персоналом</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Комплексная автоматизация">1С: Комплексная
                            автоматизация</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Управление производственным предприятием">1С:
                            Управление<br>производственным предприятием</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Управление небольшой фирмой">1С: Управление
                            небольшой<br>фирмой</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <div class="footer-menu__header">Для бюджетников</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Бухгалтерия государственного учреждения">1С:
                            Бухгалтерия<br>государственного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Зарплата и кадры бюджетного учреждения">1С:
                            Зарплата и кадры<br>бюджетного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Бюджетная отчетность">1С: Бюджетная отчетность</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Свод отчетов">1С: Свод отчетов</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Государственные и муниципальные закупки">1С:
                            Государственные и муниципальные закупки</a></li>
                </ul>
            </div>
            <div class="footer-menu__item footer-menu__item_narrow">
                <div class="footer-menu__header">Услуги</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Консультации по выбору программного обеспечения">Консультации
                            по выбору программного обеспечения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Доставка и установка">Доставка и установка</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Настройка и внедрение">Настройка и внедрение</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Учебный центр">1С: Учебный центр</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Электронная отчетность из 1С: Предприятия 8">Электронная
                            отчетность из<br>1С: Предприятия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Центр сопровождение 1С">Центр сопровождение 1С</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <div class="footer-menu__header">О компании</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Статусы компании">Статусы компании</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Наши сотрудники">Наши сотрудники</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Вакансии">Вакансии</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Новости">Новости</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Фотогалерея">Фотогалерея</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Контакты">Контакты</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer__contact">
        <div class="container container_narrow">
            <div class="footer-contact__item">
                <div class="footer-contact__city">Пятигорск</div>
                <div class="footer-contact__tel">+7 (8793) 97-34-34</div>
                <div class="footer-contact__address">ул. Коста Хетагурова 4</div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Ставрополь</div>
                <div class="footer-contact__tel">+7 (8652) 301-103</div>
                <div class="footer-contact__address">ул. Мира, 360</div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Черкесск</div>
                <div class="footer-contact__tel">+7 (8782) 26-00-44</div>
                <div class="footer-contact__address">ул. Кирова, 21а (4-этаж)</div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact-btn-wrap">
                    <div class="btn btn_main footer-contact__btn"><a href="#consul-form"
                                                                     class="link btn__link footer-contact__btn-link footer__consul-opener"
                                                                     title="Получить консультацию">Получить
                            консультацию</a></div>
                    <div class="btn btn_linear footer-contact__btn"><a href="#feeedback-form"
                                                                       class="link btn__link footer-contact__btn-link footer__feedback-opener"
                                                                       title="Написать нам">Написать нам</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__signs">
        <div class="container container_narrow">
            <div class="footer__copyright">© 2002–2017. Все права защищены.</div>
            <div class="footer__dev-sign">
                <div class="footer-dev-sign__title">Создание сайта</div>
                <div class="footer-dev-sign__name">Студия Z-labs</div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="frontend/local/app.js"></script>
</body>
</html>