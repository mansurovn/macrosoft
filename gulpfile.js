const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const postcss = require('gulp-postcss');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const debug = require('gulp-debug');
const del = require('del');
const header = require('gulp-header');
const cached = require('gulp-cached');
const combine = require('stream-combiner2').obj;
const fs = require('fs');
const data = require('gulp-data');
const normalize = require('normalize-path');
const camelCase = require('camel-case');
const pathParse = require('path-parse');
const merge = require('gulp-merge-json');
const notify = require('gulp-notify');

const isDev = (process.env.NODE_ENV === 'development');

//Начальные пути
let projectDir = normalize(fs.realpathSync('./frontend'));
let frontendDir = normalize(fs.realpathSync(`${projectDir}/local`));
let sourceDir = normalize(fs.realpathSync(`${frontendDir}`));
let buildDir = sourceDir;

//уникальный для каждого проекта
let domain = 'megatour.local';

//Маски путей для тасков
let paths = {
    styles: [`${sourceDir}/**/*.css`, `!${sourceDir}/**/*.min.css`],
    js:     [`${sourceDir}/**/*.js`, `!${sourceDir}/**/*.min.js`],
    delete: [`${buildDir}/**/*.min.{js,css}`]
};

gulp.task('common', function () {
        return gulp.src('frontend/local/common/common.scss')
            .pipe(debug({title: 'concat'}))
            .pipe(concat('app.css'))
            .pipe(debug({title: 'sass'}))
            .pipe(sass())
            .pipe(gulp.dest('frontend/local'))
            .pipe(browserSync.stream());
    });
gulp.task('sass', function(){
    return gulp.src(['frontend/vendor/slick-carousel/slick/slick.scss', 'frontend/vendor/fancybox-master/dist/jquery.fancybox.min.css', 'frontend/local/common/common.scss',  'frontend/local/**/*.scss', '!frontend/local/**/icon_prod.scss', '!frontend/local/**/fonts_prod.scss'])
        .pipe(sourcemaps.init())
            .pipe(debug({title: 'header'}))
            .pipe(header('@import "common/variables";@import "common/mixins";'))
            .pipe(debug({title: 'sass'}))
            .pipe(sass({
                includePaths: 'frontend/local'
            }).on('error', sass.logError))
            .pipe(debug({title: 'concat'}))
            .pipe(concat('app.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('frontend/local'))
        .pipe(browserSync.stream());
    });
gulp.task('mincss', gulp.series(function(){
    return gulp.src(('frontend/local/**/*.scss'), {base:'frontend/local'})
        .pipe(debug({title: 'header'}))
        .pipe(header('@import "common/variables";@import "common/mixins";'))
        .pipe(debug({title: 'sass:'}))
        .pipe(sass({
            includePaths: 'frontend/local'
        }))
        .pipe(debug({title: 'autoprefixer:'}))
        .pipe(autoprefixer())
        .pipe(debug({title: 'cssnano:'}))
        .pipe(cssnano({
            zindex: false,
            autoprefixer: false,
            reduceIdents: false
                }))
        .pipe(debug({title: 'rename:'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('frontend/local'))
    }));
gulp.task('css', gulp.series(function(){
    return gulp.src(('frontend/local/**/*.scss'), {base:'frontend/local'})
        .pipe(debug({title: 'header'}))
        .pipe(header('@import "common/variables";@import "common/mixins";'))
        .pipe(debug({title: 'sass:'}))
        .pipe(sass({
            includePaths: 'frontend/local'
        }))
        .pipe(debug({title: 'autoprefixer:'}))
        .pipe(autoprefixer())
        .pipe(gulp.dest('frontend/local'))
}));
gulp.task('php', function(){
        return gulp.src('index.php')
            .pipe(browserSync.stream());
    });
gulp.task('clean', function(done) {
    del.sync('frontend/local/app.js');
    done();
});
gulp.task('cleancss', gulp.series(function(done) {
    del.sync(['frontend/local/**/*.css','!frontend/local/app.css', '!frontend/local/icon/fontello/**/*.css']);
    done();
}));
gulp.task('cleanminscript', gulp.series(function(done) {
    del.sync('frontend/local/**/*.min.js');
    done();
}));
gulp.task('cleanall', gulp.parallel('cleancss', 'cleanminscript', function (done) {
    del.sync(['frontend/local/frontend.json','frontend/local/app.{css,js}']);
    done();
}));

gulp.task('script', gulp.series(function(){
    return gulp.src(['frontend/vendor/jquery-3.2.1.min.js', 'frontend/vendor/fancybox-master/dist/jquery.fancybox.min.js','frontend/vendor/slick-carousel/slick/slick.min.js', 'frontend/local/**/common.js', 'frontend/local/**/*.js', '!frontend/local/app.js', '!frontend/local/**/*.min.js'])
        .pipe(sourcemaps.init())
            .pipe(debug({title: "concat"}))
            .pipe(concat('app.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('frontend/local'))
        .pipe(browserSync.stream());
    }));

gulp.task('default', function(){
        browserSync.init({
            proxy: "http://macrosoft.dev/"
        });
        gulp.watch('frontend/local/**/*.scss', gulp.series('sass')).on('change', browserSync.reload);
        gulp.watch('*.php').on('change', browserSync.reload);
        gulp.watch(['!frontend/local/app.js','frontend/local/**/*.js'], gulp.series('clean','script')).on('change', browserSync.reload);
    });
gulp.task('minscript', gulp.series(function(){
    return gulp.src(['!frontend/local/app.js', '!frontend/local/**/*.min.js', 'frontend/local/**/*.js'])
        .pipe(debug({title: 'uglify:'}))
        .pipe(uglify())
        .pipe(debug({title: 'rename:'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('frontend/local'))
    }));
gulp.task('json', gulp.series(function(){
    return gulp.src(['frontend/local/*/*.json'])
        .pipe(rename({
            basename: 'dep'
        }))
        .pipe(gulp.dest('frontend/local'))
}));
gulp.task('cleanjson', function(done){
    del.sync(['frontend/local/**/*.json','!frontend/local/*/dep.json']);
    done();
});
gulp.task('frontend-json', function() {

    return combine(
        gulp.src(`${sourceDir}/**/dep.json`, {base: sourceDir, dot: true}),
        merge({
                fileName: 'frontend.json',
                edit: (parsedJson, file) =>  {

                let filePath, files, libName, rootDir, jsonResult;

                filePath = pathParse(normalize(file.history[0]));
                rootDir = file.base.substr(file.base.indexOf(projectDir) + projectDir.length);
                filePath.relDir = filePath.dir.substr(filePath.dir.indexOf(sourceDir) + sourceDir.length);
                files = fs.readdirSync(filePath.dir);
                libName = camelCase(filePath.relDir);

                jsonResult = {};

                jsonResult[libName] = {
                    css: files.filter((value) => {
                        return ((value.length === (value.indexOf(".css") + ".css".length)) && value.indexOf(".min.css") === -1)
            }),
                js: files.filter((value) => {
                    return ((value.length === (value.indexOf(".js") + ".js".length)) && value.indexOf(".min.js") === -1)
            }),
                rel: parsedJson
            };

                jsonResult[libName].css = jsonResult[libName].css.map( (value) => `${rootDir}/${value}` );
                jsonResult[libName].js = jsonResult[libName].js.map( (value) => `${rootDir}/${value}` );

                return jsonResult;
            }
            }),
                gulp.dest(sourceDir)
                ).on("error", notify.onError());

});
gulp.task('prod', gulp.series(gulp.parallel('clean','cleancss','cleanminscript'), gulp.parallel('css', 'mincss', 'minscript', 'sass'), 'script'));

