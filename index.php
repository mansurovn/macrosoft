<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="frontend/local/app.css">
    <title>Index</title>
</head>
<body class="body">
<header class="header">
    <div class="container container_narrow">
        <div class="header__item">
            <a href="#" class="header__brand header__logo" title="Главная"></a>
        </div>
        <div class="header__item">
            <div class="header__slogan">Простые решения<br>для вашего бизнеса</div>
        </div>
        <div class="header__item">
            <div class="header__location-and-tel">
                <div class="header__location">
                    <button class="header-location__btn header__title"><span class="header-location__showcase"><span
                                    class="header-location__link icon icon_arrow-angle-down">Пятигорск</span></span>
                    </button>
                    <div class="header-location__list">
                        <div class="header-location__item header-location__item_active">Пятигорск</div>
                        <div class="header-location__item">Ставрополь</div>
                        <div class="header-location__item">Черкесск</div>
                    </div>
                </div>
                <div class="header__text header__tel">+7 (8793) 97-34-34</div>
            </div>
        </div>
        <div class="header__item">
            <div class="header__mail">
                <div class="header__title">Эл. почта</div>
                <div class="header__text">info@mskmv.ru</div>
            </div>
        </div>
        <div class="header__item">
            <a href="#" class="header__brand header__1c" title="1C:Центр Сопровождения"></a>
        </div>
    </div>
</header>
<nav class="nav">
    <div class="container container_wide">
        <ul class="nav__list clearfix">
            <li class="nav__item">
                <a href="#" class="link nav__link" title="О нас"><span class="nav__undescore">О нас</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Продукты"><span class="nav__undescore">Продукты</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Услуги"><span class="nav__undescore">Услуги</span></a>
            </li>
            <li class="nav__item  nav__trigger nav__item_angle">
                <a href="#" class="link nav__link" title="1С:Сопровождение"><span class="nav__undescore">1С:Сопровождение</span></a>
                <div class="nav__sub">
                    <ul class="nav-sub__list">
                        <li class="nav-sub__item"><a href="#" class="link nav-sub__link"
                                                     title="Услуги и сервисы для пользователей ИТС">Услуги и сервисы для
                                пользователей ИТС</a></li>
                        <li class="nav-sub__item"><a href="#" class="link nav-sub__link"
                                                     title="Линия консультаций для пользователей ИТС">Линия консультаций
                                для пользователей ИТС</a></li>
                        <li class="nav-sub__item"><a href="#" class="link nav-sub__link"
                                                     title="Условия заключения договора ИТС">Условия заключения договора
                                ИТС</a></li>
                        <li class="nav-sub__item"><a href="#" class="link nav-sub__link" title="ИТС: ПРОФ">ИТС: ПРОФ</a>
                        </li>
                        <li class="nav-sub__item"><a href="#" class="link nav-sub__link" title="ИТС: Бюджет">ИТС:
                                Бюджет</a></li>
                        <li class="nav-sub__item"><a href="#" class="link nav-sub__link" title="ИТС: Строительство">ИТС:
                                Строительство</a></li>
                        <li class="nav-sub__item"><a href="#" class="link nav-sub__link" title="ИТС: Медицина">ИТС:
                                Медицина</a></li>
                    </ul>
                </div>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Учебный центр"><span class="nav__undescore">1С:Учебный центр</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Отчетность"><span
                            class="nav__undescore">1С:Отчетность</span></a>
            </li>
        </ul>
    </div>
</nav>
<div class="main">
    <div class="banner-on-main visibility">
        <div class="container container_wide banner-on-main__container">
            <div class=" banner-on-main__content clearfix">
                <div class="banner-on-main-panel-wrap">
                    <a href="#" class="banner-on-main__panel"></a>
                </div>
                <div class="banner-on-main-slider-wrap">
                    <div class="banner-on-main__slider">
                        <a href="#" class="banner-on-main__slide clearfix">
                        </a>
                        <a href="#" class="banner-on-main__slide clearfix">
                        </a>
                        <a href="#" class="banner-on-main__slide clearfix">
                        </a>
                        <a href="#" class="banner-on-main__slide clearfix">
                        </a>
                        <a href="#" class="banner-on-main__slide clearfix">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="trigger-section">
        <div class="container container_narrow">
            <div class="trigger-wrap">
                <a href="#" class="link trigger">
                    <span class="trigger__icon icon icon_hat"></span>
                    <span class="trigger__title-and-text">
                        <span class="trigger__title">Обучаем бухгалтерии и 1С программированию</span>
                        <span class="trigger__text">Подробнее о курсах</span>
                    </span>
                </a>
            </div>
            <div class="trigger-wrap">
                <a href="#" class="link trigger">
                    <span class="trigger__icon trigger__icon_small icon icon_cart"></span>
                    <span class="trigger__title-and-text">
                        <span class="trigger__title">Официальные дистрибьюторы программных продуктов 1С</span>
                        <span class="trigger__text">Перейти в каталог</span>
                    </span>
                </a>
            </div>
            <div class="trigger-wrap">
                <a href="#" class="link trigger">
                    <span class="trigger__icon icon icon_headphone"></span>
                    <span class="trigger__title-and-text">
                        <span class="trigger__title">Круглосуточная поддержка клиентов </span>
                        <span class="trigger__text">Подробнее о компании</span>
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="tabs-on-main visibility">
        <div class="container container_narrow">
            <div class="tabs-on-main__tablist clearfix" role="tablist">
                <div class="tabs-on-main-tab-wrap">
                    <a href="#tabs-on-main__tabpanel1" class="link tabs-on-main__tab tabs-on-main__tab_active"
                       title="Основные услуги">Основные услуги</a>
                </div>
                <div class="tabs-on-main-tab-wrap">
                    <a href="#tabs-on-main__tabpanel2" class="link tabs-on-main__tab"
                       title="Курсы и тренинги">Курсы и тренинги</a>
                </div>
                <div class="tabs-on-main-tab-wrap">
                    <a href="#tabs-on-main__tabpanel3" class="link tabs-on-main__tab"
                       title="Семинары и мастер-классы"><span
                                class="sticker sticker__bar sticker__bar_new tabs-on-main_sticker">Новинка</span>Семинары
                        и мастер-классы</a>
                </div>
            </div>
            <div class="tabs-on-main__tabpanels">
                <div class="tabs-on-main__tabpanel  tabs-on-main__tabpanel_active" id="tabs-on-main__tabpanel1"
                     role="tabpanel">
                    <div class="clearfix">
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_note"></div>
                            <div class="tabs-on-main__text">Проведение консультаций по выбору программного обеспечения
                            </div>
                        </a>
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_cmd"></div>
                            <div class="tabs-on-main__text">Доставка и установка программного обеспечения</div>
                        </a>
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_gear"></div>
                            <div class="tabs-on-main__text">Работы по настройке<br>и внедрению</div>
                        </a>
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_user"></div>
                            <div class="tabs-on-main__text">Обучение персонала работе с системой</div>
                        </a>
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_sign"></div>
                            <div class="tabs-on-main__text">Информационно-<br>технологическое сопровождение</div>
                        </a>
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_chart"></div>
                            <div class="tabs-on-main__text">Сопровождение сданной<br>в эксплуатацию системы</div>
                        </a>
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_chart"></div>
                            <div class="tabs-on-main__text">Сопровождение сданной<br>в эксплуатацию системы</div>
                        </a>
                    </div>
                    <div class="tabs-on-main__footer">
                        <div class="btn btn_main tabs-on-main__btn"><a href="#"
                                                                       class="link btn__link tabs-on-main__btn-link"
                                                                       title="Смортеть все услуги">Смортеть все
                                услуги</a></div>
                    </div>
                </div>
                <div class="tabs-on-main__tabpanel  clearfix" id="tabs-on-main__tabpanel2" role="tabpanel">
                    <div class="clearfix">
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_note"></div>
                            <div class="tabs-on-main__text">Проведение консультаций по выбору программного обеспечения</div>
                        </a>
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_cmd"></div>
                            <div class="tabs-on-main__text">Доставка и установка программного обеспечения</div>
                        </a>
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_gear"></div>
                            <div class="tabs-on-main__text">Работы по настройке<br>и внедрению</div>
                        </a>
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_gear"></div>
                            <div class="tabs-on-main__text">Работы по настройке<br>и внедрению</div>
                        </a>
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_user"></div>
                            <div class="tabs-on-main__text">Обучение персонала работе с системой</div>
                        </a>
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_sign"></div>
                            <div class="tabs-on-main__text">Информационно-<br>технологическое сопровождениеИнформационно-<br>технологическое сопровождениеИнформационно-<br>технологическое сопровождениеИнформационно-<br>технологическое сопровождениеИнформационно-<br>технологическое сопровождениеИнформационно-<br>технологическое сопровождение</div>
                        </a>
                        <a href="#" class="link tabs-on-main__block">
                            <div class="tabs-on-main__icon icon icon_chart"></div>
                            <div class="tabs-on-main__text">Сопровождение сданной<br>в эксплуатацию системы</div>
                        </a>
                    </div>
                    <div class="tabs-on-main__footer">
                        <div class="btn btn_main tabs-on-main__btn"><a href="#"
                                                                       class="link btn__link tabs-on-main__btn-link"
                                                                       title="Смортеть все услуги">Смортеть все
                                услуги</a></div>
                    </div>
                </div>
                <div class="tabs-on-main__tabpanel  clearfix" id="tabs-on-main__tabpanel3"
                     role="tabpanel"></div>
            </div>
        </div>
    </div>
    <div class="minibanner-section">
        <div class="container container_wide">
            <div class="minibanner minibanner_v1"></div>
            <div class="minibanner minibanner_v2"></div>
            <div class="minibanner minibanner_v3"></div>
            <div class="minibanner minibanner_v4"></div>
        </div>
    </div>
    <section class="press visibility">
        <div class="press__head">
            <div class="container container_narrow">
                <h1 class="press__header">Пресс-центр</h1>
                <div class="press-slider__toolbar clearfix">
                </div>
                <div class="btn btn_linear press__btn">
                    <a href="#" class="link btn__link press__btn-link">Смортеть все новости</a>
                </div>
            </div>
        </div>
        <div class="press__body">
            <div class="container container_wide">
                <div class="press__slider">
                    <div class="press__slide">
                        <div class="press-slide__date">27.07</div>
                        <div class="press-slide__text">Электронная подпись для ЕГАИС федеральной службы по
                            урегулированию
                        </div>
                    </div>
                    <div class="press__slide">
                        <div class="press-slide__date">31.07</div>
                        <div class="press-slide__text">Семинар «Положение о учётной политике учреждения» для гос.
                            учрежедений
                        </div>
                    </div>
                    <div class="press__slide">
                        <div class="press-slide__date">14.08</div>
                        <div class="press-slide__text">Электронная подпись для ЕГАИС федеральной службы</div>
                    </div>
                    <div class="press__slide">
                        <div class="press-slide__date">12.09</div>
                        <div class="press-slide__text">Семинар «Положение о учётной политике учраждения» для гос.
                            учрежедений и ещё немного текста для проверки
                        </div>
                    </div>
                    <div class="press__slide">
                        <div class="press-slide__date">12.09</div>
                        <div class="press-slide__text">Семинар «Положение о учётной политике учраждения» для гос.
                            учрежедений и ещё немного текста для проверки
                        </div>
                    </div>
                    <div class="press__slide">
                        <div class="press-slide__date">12.09</div>
                        <div class="press-slide__text">Семинар «Положение о учётной политике учраждения» для гос.
                            учрежедений и ещё немного текста для проверки
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="review visibility">
        <div class="container container_narrow">
            <div class="review__head">
                <h1 class="h1 review__header">Отзывы клиентов</h1>
            </div>
            <div class="review__body">
                <div class="review__slider">
                    <div class="review-slide-wrap">
                        <div class="review__slide">
                            <div class="review-slide__img"></div>
                            <div class="review-slide__title-and-text">
                                <div class="review-slide__title">Вирта Татьяна Александровна</div>
                                <div class="review-slide__text">Статус Центра Сертифицированного Обучения позволяет
                                    Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                    курсам, разработанным фирмой «1С». Статус Центра Сертифицированного Обучения позволяет
                                    Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                    курсам, разработанным фирмой «1С».
                                    Статус Центра Сертифицированного Обучения позволяет
                                    Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                    курсам, разработанным фирмой «1С».
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="review-slide-wrap">
                        <div class="review__slide">
                            <div class="review-slide__img"></div>
                            <div class="review-slide__title-and-text">
                                <div class="review-slide__title">Вирта Татьяна Александровна</div>
                                <div class="review-slide__text">Статус Центра Сертифицированного Обучения позволяет
                                    Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                    курсам, разработанным фирмой «1С».Статус Центра Сертифицированного Обучения позволяет
                                    Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                    курсам, разработанным фирмой «1С».Статус Центра Сертифицированного Обучения позволяет
                                    Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                    курсам, разработанным фирмой «1С».Статус Центра Сертифицированного Обучения позволяет
                                    Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                    курсам, разработанным фирмой «1С».
                                    Статус Центра Сертифицированного Обучения позволяет
                                    Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                    курсам, разработанным фирмой «1С».
                                    Статус Центра Сертифицированного Обучения позволяет
                                    Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                    курсам, разработанным фирмой «1С».
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="review-slide-wrap">
                        <div class="review__slide">
                            <div class="review-slide__img"></div>
                            <div class="review-slide__title-and-text">
                                <div class="review-slide__title">Вирта Татьяна Александровна</div>
                                <div class="review-slide__text">Статус Центра Сертифицированного Обучения позволяет
                                    Учебному Центру обучать пользователей работе с системой программ «1С:Предприятие 8»,<br>по
                                    курсам, разработанным фирмой «1С».
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="review__footer">
                <button class="btn btn_linear review__btn review__expander"><span
                            class="link btn__link review__btn-link">Читать отзыв целиком</span></button>
                <div class="btn btn_main review__btn review__btn_main"><a href="#review__form"
                                                                          class="link btn__link review__btn-link review__pop-up-opener">Оставить
                        свой отзыв</a></div>
            </div>
        </div>
        <div class="review__form feedback-form-modal-wrap" id="review__form">
            <div class="feedback-form__title">
                <h1 class="feedback-form__title-value">Добавить отзыв</h1>
            </div>
            <form action="#" class="feedback-form feedback-fom_modal">
                <div class="feedback-form__group">
                    <input type="text" class="feedback-form__control feedback-form__control_type_text"
                           placeholder="Введите ФИО">
                </div>
                <div class="feedback-form__group">
                    <textarea
                            class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_tall"
                            placeholder="Текст отзыва"></textarea>
                </div>
                <div class="feedback-form__footer">
                    <div class="feedback-form__group clearfix">
                        <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                            <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                            <span class="custom-control__indicator icon icon_checked"></span>
                            <span class="custom-control__name">Я согласен на обработку</span> <a href="#"
                                                                                                 class="link custom-control__link"
                                                                                                 title="Политика конфиденциальности">персональных
                                данных</a>
                        </label>
                    </div>
                    <div class="feedback-form__group">
                        <button class="btn btn_main feedback-form__submit"><span class="btn__link">Добавить отзыв</span>
                        </button>
                        <div class="feedback-form__footnote"></div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>

<footer class="footer">
    <div class="footer__consul-form feedback-form-modal-wrap" id="consul-form">
        <div class="feedback-form__title">
            <h1 class="feedback-form__title-value">Нужна консультация?</h1>
            <div class="feedback-form__desc">Бесплатно проконсультироваться, уточнить цены и<br>заказать решение можно у
                специалистов нашей фирмы
            </div>
        </div>
        <form action="#" class="feedback-form feedback-form_modal">
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text"
                       placeholder="Ваше имя">
            </div>
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text"
                       placeholder="Ваш телефон">
            </div>
            <div class="feedback-form__group">

                <select class="feedback-form__control_type_select" id="sel">
                    <option value="">Выберите город</option>
                    <option class="feedback-form__control_type_option">Город 1</option>
                    <option class="feedback-form__control_type_option">Город 2</option>
                    <option class="feedback-form__control_type_option">Город 3</option>
                </select>
            </div>
            <div class="feedback-form__group">
                <textarea
                        class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short"
                        placeholder="Ваш вопрос"></textarea>
            </div>
            <div class="form__footer">
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#"
                                                                                             class="link custom-control__link"
                                                                                             title="Политика конфиденциальности">персональных
                            данных</a>
                    </label>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span
                                class="btn__link">Получить консультацию</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>
            </div>
        </form>

    </div>
    <div class="footer__feedback-form feedback-form-modal-wrap" id="feeedback-form">
        <div class="feedback-form__title">
            <h1 class="feedback-form__title-value">Написать нам</h1>
        </div>
        <form action="#" class="feedback-form feedback-form_modal">
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text"
                       placeholder="Ваше имя">
            </div>
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text"
                       placeholder="Ваш email">
            </div>
            <div class="feedback-form__group">
                <textarea
                        class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short"
                        placeholder="Ваш вопрос"></textarea>
            </div>
            <div class="form__footer">
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#"
                                                                                             class="link custom-control__link"
                                                                                             title="Политика конфиденциальности">персональных
                            данных</a>
                    </label>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span
                                class="btn__link">Отправить сообщение</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>

            </div>
        </form>
    </div>
    <div class="footer__menu clearfix">
        <div class="container container_narrow">
            <div class="footer-menu__item">
                <a href="#" class="link footer-menu__header" title="1С: Предприятие 8">1С: Предприятие 8</a>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Бухгалтерия 8">1С: Бухгалтерия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Управление торговлей">1С: Управление торговлей</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Зарплата и Управление персоналом">1С: Зарплата и
                            Управление персоналом</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Комплексная автоматизация">1С: Комплексная
                            автоматизация</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Управление производственным предприятием">1С:
                            Управление<br>производственным предприятием</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Управление небольшой фирмой">1С: Управление
                            небольшой<br>фирмой</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <a href="#" class="link footer-menu__header" title="Для бюджетников">Для бюджетников</a>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Бухгалтерия государственного учреждения">1С:
                            Бухгалтерия<br>государственного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Зарплата и кадры бюджетного учреждения">1С:
                            Зарплата и кадры<br>бюджетного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Бюджетная отчетность">1С: Бюджетная отчетность</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Свод отчетов">1С: Свод отчетов</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Государственные и муниципальные закупки">1С:
                            Государственные и муниципальные закупки</a></li>
                </ul>
            </div>
            <div class="footer-menu__item footer-menu__item_narrow">
                <a href="#" class="link footer-menu__header" title="Услуги">Услуги</a>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Консультации по выбору программного обеспечения">Консультации
                            по выбору программного обеспечения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Доставка и установка">Доставка и установка</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Настройка и внедрение">Настройка и внедрение</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="1С: Учебный центр">1С: Учебный центр</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Электронная отчетность из 1С: Предприятия 8">Электронная
                            отчетность из<br>1С: Предприятия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Центр сопровождение 1С">Центр сопровождение 1С</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <a href="#" class="link footer-menu__header" title="О компании">О компании</a>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Статусы компании">Статусы компании</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link"
                                                          title="Наши сотрудники">Наши сотрудники</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Вакансии">Вакансии</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Новости">Новости</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Фотогалерея">Фотогалерея</a>
                    </li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Контакты">Контакты</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer__contact">
        <div class="container container_narrow">
            <div class="footer-contact__item">
                <div class="footer-contact__city">Пятигорск</div>
                <div class="footer-contact__tel">+7 (8793) 97-34-34</div>
                <div class="footer-contact__address">ул. Коста Хетагурова 4</div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Ставрополь</div>
                <div class="footer-contact__tel">+7 (8652) 301-103</div>
                <div class="footer-contact__address">ул. Мира, 360</div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Черкесск</div>
                <div class="footer-contact__tel">+7 (8782) 26-00-44</div>
                <div class="footer-contact__address">ул. Кирова, 21а (4-этаж)</div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact-btn-wrap">
                    <div class="btn btn_main footer-contact__btn"><a href="#consul-form"
                                                                     class="link btn__link footer-contact__btn-link footer__consul-opener"
                                                                     title="Получить консультацию">Получить
                            консультацию</a></div>
                    <div class="btn btn_linear footer-contact__btn"><a href="#feeedback-form"
                                                                       class="link btn__link footer-contact__btn-link footer__feedback-opener"
                                                                       title="Написать нам">Написать нам</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__signs">
        <div class="container container_narrow">
            <div class="footer__copyright">© 2002–2017. Все права защищены.</div>
            <div class="footer__dev-sign">
                <div class="footer-dev-sign__title">Создание сайта</div>
                <div class="footer-dev-sign__name">Студия Z-labs</div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="frontend/local/app.js"></script>
</body>
</html>