<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="frontend/local/app.css">
    <script src="//api-maps.yandex.ru/2.1/?load=package.standard&lang=ru-RU&onerror=yMapErrorHandler"></script>
    <title>Contact</title>
</head>
<body class="body">
<header class="header">
    <div class="container container_narrow">
        <div class="header__item">
            <a href="#" class="header__brand header__logo" title="Главная"></a>
        </div>
        <div class="header__item">
            <div class="header__slogan">Простые решения<br>для вашего бизнеса</div>
        </div>
        <div class="header__item">
            <div class="header__location-and-tel">
                <div class="header__location">
                    <button class="header-location__btn header__title"><span class="header-location__showcase"><span class="header-location__link icon icon_arrow-angle-down">Пятигорск</span></span></button>
                    <div class="header-location__list">
                        <div class="header-location__item header-location__item_active">Пятигорск</div>
                        <div class="header-location__item">Ставрополь</div>
                        <div class="header-location__item">Черкесск</div>
                    </div>
                </div>
                <div class="header__text header__tel">+7 (8793) 97-34-34 </div>
            </div>
        </div>
        <div class="header__item">
            <div class="header__mail">
                <div class="header__title">Эл. почта</div>
                <div class="header__text">info@mskmv.ru</div>
            </div>
        </div>
        <div class="header__item">
            <a href="#" class="header__brand header__1c" title="1C:Центр Сопровождения"></a>
        </div>
    </div>
</header>
<nav class="nav">
    <div class="container container_narrow">
        <ul class="nav__list clearfix">
            <li class="nav__item">
                <a href="#" class="link nav__link" title="О нас"><span class="nav__undescore">О нас</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Продукты"><span class="nav__undescore">Продукты</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="Услуги"><span class="nav__undescore">Услуги</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Сопровождение"><span class="nav__undescore">1С:Сопровождение</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Учебный центр"><span class="nav__undescore">1С:Учебный центр</span></a>
            </li>
            <li class="nav__item">
                <a href="#" class="link nav__link" title="1С:Отчетность"><span class="nav__undescore">1С:Отчетность</span></a>
            </li>
        </ul>
    </div>
</nav>
<div class="breadcrumbs">
    <div class="container container_narrow">
        <a href="#" class="link breadcrumbs__link icon icon_arrow-right" title="Главная">Главная</a>
        <a href="#" class="link breadcrumbs__link" title="Контакты">Контакты</a>
    </div>
</div>
<div class="main clearfix">
    <div class="container container_narrow">
        <h1 class="h1">Контакты</h1>
        <div class="content">
            <div class="contacts">
                <div class="contacts__panels clearfix">
                    <div class="contacts__panel">
                        <div class="contacts-panel__title">Пятигорск</div>
                        <div class="contacts-panel__tels contacts-panel__icon icon icon_phone">
                            <div class="contacts-panel__tel">+7 (8793) 97-34-34</div>
                        </div>
                        <div class="contacts-panel__mails contacts-panel__icon icon icon_mail">
                            <a href="mailto:stv@mskmv.ru" class="link contacts-panel__mail" title="general@mskmv.ru">general@mskmv.ru</a>
                        </div>
                        <div class="contacts-panel__addresses contacts-panel__icon icon icon_pin">
                            <div class="contacts-panel__address">ул. Коста Хетагурова, 4</div>
                        </div>
                        <div class="contacts-panel__footer">
                            <button class="contacts-panel__link" data-lat="44.040433" data-lon="43.069524" data-hint-content="Пятигорск" data-balloon-content="<b>Пятигорск</b>">Показать на карте</button>
                        </div>
                    </div>
                    <div class="contacts__panel">
                        <div class="contacts-panel__title">Ставрополь</div>
                        <div class="contacts-panel__tels contacts-panel__icon icon icon_phone">
                            <div class="contacts-panel__tel">+7 (8652) 301-103</div>
                        </div>
                        <div class="contacts-panel__mails contacts-panel__icon icon icon_mail">
                            <a href="mailto:stv@mskmv.ru" class="link contacts-panel__mail" title="stv@mskmv.ru">stv@mskmv.ru</a>
                        </div>
                        <div class="contacts-panel__addresses contacts-panel__icon icon icon_pin">
                            <div class="contacts-panel__address">ул. Мира, 360</div>
                        </div>
                        <div class="contacts-panel__footer">
                            <button class="contacts-panel__link" data-lat="45.035079" data-lon="41.949379" data-hint-content="Ставрополь" data-balloon-content="<b>Ставрополь</b>">Показать на карте</button>
                        </div>
                    </div>
                    <div class="contacts__panel">
                        <div class="contacts-panel__title">Черкесск</div>
                        <div class="contacts-panel__tels contacts-panel__icon icon icon_phone">
                            <div class="contacts-panel__tel">+7 (8782) 26-00-44 </div>
                        </div>
                        <div class="contacts-panel__mails contacts-panel__icon icon icon_mail">
                            <a href="mailto:kchr@mskmv.ru" class="link contacts-panel__mail" title="kchr@mskmv.ru">kchr@mskmv.ru</a>
                        </div>
                        <div class="contacts-panel__addresses contacts-panel__icon icon icon_pin">
                            <div class="contacts-panel__address">ул. Кирова, 21а (4 этаж)</div>
                        </div>
                        <div class="contacts-panel__footer">
                            <button class="contacts-panel__link" data-lat="44.227647" data-lon="42.054820" data-hint-content="Черкесск" data-balloon-content="<b>Черкесск</b>">Показать на карте</button>
                        </div>
                    </div>
                </div>
                <div class="contacts__mode">
                    <div class="contacts-mode__title">Мы работаем по будням</div>
                    <div class="contacts-mode__time">с 9:00 до 18:00</div>
                </div>
                <div class="contacts__map">
                    <div class="contact-map__canvas" id="YMap" style="width: 100%; height: 433px;"></div>
                    <div class="contact-map__preloader"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="footer__consul-form feedback-form-modal-wrap" id="consul-form">
        <div class="feedback-form__title
            <h1 class="feedback-form__title-valueНужна консультация?</h1>
            <div class="feedback-form__desc">Бесплатно проконсультироваться, уточнить цены и<br>заказать решение можно у специалистов нашей фирмы</div>
        </div>

        <form action="#" class="feedback-form feedback-form_modal">
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
            </div>
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш телефон">
            </div>
            <div class="feedback-form__group">
                <div class="feedback-form__control_type_select"><span class="feedback-form__control_type_current-option">Выберите город</span><span class="feedback-form__icon icon icon_arrow-angle-down"></span></div>
                <ul class="feedback-form__control_type_options">
                    <li class="feedback-form__control_type_option">Город 1</li>
                    <li class="feedback-form__control_type_option">Город 2</li>
                    <li class="feedback-form__control_type_option">Город 3</li>
                </ul>
            </div>
            <div class="feedback-form__group">
                <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
            </div>
            <div class="form__footer">
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                    </label>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span class="btn__link">Получить консультацию</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>
            </div>
        </form>

    </div>
    <div class="footer__feedback-form feedback-form-modal-wrap" id="feeedback-form">
        <div class="feedback-form__title
            <h1 class="feedback-form__title-valueНаписать нам</h1>
        </div>
        <form action="#" class="feedback-form feedback-form_modal">
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваше имя">
            </div>
            <div class="feedback-form__group">
                <input type="text" class="feedback-form__control feedback-form__control_type_text" placeholder="Ваш email">
            </div>
            <div class="feedback-form__group">
                <textarea class="feedback-form__control feedback-form__control_type_textarea feedback-form__control_type_textarea_short" placeholder="Ваш вопрос"></textarea>
            </div>
            <div class="form__footer">
                <div class="feedback-form__group clearfix">
                    <label for="form__checkbox" class="custom-control custom-control_type_checkbox">
                        <input type="checkbox" class="custom-control__input" id="form__checkbox" checked>
                        <span class="custom-control__indicator icon icon_checked"></span>
                        <span class="custom-control__name">Я согласен на обработку</span> <a href="#" class="link custom-control__link" title="Политика конфиденциальности">персональных данных</a>
                    </label>
                </div>
                <div class="feedback-form__group">
                    <button class="btn btn_main feedback-form__submit"><span class="btn__link">Отправить сообщение</span></button>
                    <div class="feedback-form__footnote"></div>
                </div>

            </div>
        </form>
    </div>
    <div class="footer__menu clearfix">
        <div class="container container_narrow">
            <div class="footer-menu__item">
                <div class="footer-menu__header">1С: Предприятие 8</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бухгалтерия 8">1С: Бухгалтерия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление торговлей">1С: Управление торговлей</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Зарплата и Управление персоналом">1С: Зарплата и Управление персоналом</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Комплексная автоматизация">1С: Комплексная автоматизация</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление производственным предприятием">1С: Управление<br>производственным предприятием</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Управление небольшой фирмой">1С: Управление небольшой<br>фирмой</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <div class="footer-menu__header">Для бюджетников</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бухгалтерия государственного учреждения">1С: Бухгалтерия<br>государственного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Зарплата и кадры бюджетного учреждения">1С: Зарплата и кадры<br>бюджетного учреждения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Бюджетная отчетность">1С: Бюджетная отчетность</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Свод отчетов">1С: Свод отчетов</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Государственные и муниципальные закупки">1С: Государственные и муниципальные закупки</a></li>
                </ul>
            </div>
            <div class="footer-menu__item footer-menu__item_narrow">
                <div class="footer-menu__header">Услуги</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Консультации по выбору программного обеспечения">Консультации по выбору программного обеспечения</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Доставка и установка">Доставка и установка</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Настройка и внедрение">Настройка и внедрение</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="1С: Учебный центр">1С: Учебный центр</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Электронная отчетность из 1С: Предприятия 8">Электронная отчетность из<br>1С: Предприятия 8</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Центр сопровождение 1С">Центр сопровождение 1С</a></li>
                </ul>
            </div>
            <div class="footer-menu__item">
                <div class="footer-menu__header">О компании</div>
                <ul class="footer-menu__list">
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Статусы компании">Статусы компании</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Наши сотрудники">Наши сотрудники</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Вакансии">Вакансии</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Новости">Новости</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Фотогалерея">Фотогалерея</a></li>
                    <li class="footer-menu-list__item"><a href="#" class="link footer-menu__link" title="Контакты">Контакты</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer__contact">
        <div class="container container_narrow">
            <div class="footer-contact__item">
                <div class="footer-contact__city">Пятигорск</div>
                <div class="footer-contact__tel">+7 (8793) 97-34-34 </div>
                <div class="footer-contact__address">ул. Коста Хетагурова 4 </div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Ставрополь</div>
                <div class="footer-contact__tel">+7 (8652) 301-103 </div>
                <div class="footer-contact__address">ул. Мира, 360</div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact__city">Черкесск</div>
                <div class="footer-contact__tel">+7 (8782) 26-00-44</div>
                <div class="footer-contact__address">ул. Кирова, 21а (4-этаж) </div>
            </div>
            <div class="footer-contact__item">
                <div class="footer-contact-btn-wrap">
                    <div class="btn btn_main footer-contact__btn"><a href="#consul-form" class="link btn__link footer-contact__btn-link footer__consul-opener" title="Получить консультацию">Получить консультацию</a></div>
                    <div class="btn btn_linear footer-contact__btn"><a href="#feeedback-form" class="link btn__link footer-contact__btn-link footer__feedback-opener" title="Написать нам">Написать нам</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__signs">
        <div class="container container_narrow">
            <div class="footer__copyright">© 2002–2017. Все права защищены. </div>
            <div class="footer__dev-sign">
                <div class="footer-dev-sign__title">Создание сайта</div>
                <div class="footer-dev-sign__name">Студия Z-labs</div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="frontend/local/app.js"></script>
</body>
</html>