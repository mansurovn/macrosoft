$(document).ready(function () {
    // Списки в панелях
    var triggers = $('.pop-up__trigger');
    var lists = $('.pop-up__list');
    var isTabOpen = false;

    if (lists.length) {
        triggers.on('click', function(event){
            var curList = $(this).find('.pop-up');
            var curTrigger = $(this);
            //curList.fadeIn('fast');
            if (curList.hasClass('pop-up_active')) {
                listHide(curList);
            } else {
                listShow(curList);
            }
            curList.find('.pop-up-list__item').on('click', function(event){
                curTrigger.find('.pop-up-list__current').text($(this).text());
                curList.find('.pop-up-list__item').removeClass('pop-up-list__item_active');
                $(this).addClass('pop-up-list__item_active');
                listHide(curList);
                return false;
            });

            $(this).on('mouseleave', function(event){
                listHide(curList);
            });
        });
    }

    function listHide(curList) {
        curList.removeClass('pop-up_in');
        setTimeout(function (event) {
            curList.removeClass('pop-up_active');
        }, 200);
    }

    function listShow(curList) {
        curList.addClass('pop-up_active');
        setTimeout(function (event) {
            curList.addClass('pop-up_in');
        }, 20)
    }
});