$(document).ready(function () {
    var tabpanels = $('.tabs-on-main__tabpanels');
    var tabpanel = $('.tabs-on-main__tabpanel');
    var textBlock = $('.tabs-on-main__block');
    var link = $('.tabs-on-main__tab');

    setTimeout(function(){
        tabpanel.each(function( index, value){
            $(this).find('.tabs-on-main__block').equality();
            $(this).addClass('tabs-on-main__tabpanel__hide');
           if (index == 0) {
               $(this).addClass('tabs-on-main__tabpanel_active tabs-on-main__tabpanel_in');
           }
        });
    }, 20);

    link.on('click', function(event){
        event.preventDefault();

        var nextPanel = $($(this).attr('href'));
        var curPanel = $('.tabs-on-main__tabpanel_active');

        $('.tabs-on-main__tab').removeClass('tabs-on-main__tab_active');
        $(this).addClass('tabs-on-main__tab_active');

        curPanel.removeClass('tabs-on-main__tabpanel_in');
        setTimeout(function(){
            curPanel.removeClass('tabs-on-main__tabpanel_active');
            nextPanel.addClass('tabs-on-main__tabpanel_active');
        }, 200);
        setTimeout(function(){
            tabpanels.height(nextPanel.outerHeight());
        }, 220);
        setTimeout(function(){
            nextPanel.addClass('tabs-on-main__tabpanel_in');
        }, 300);
    });
});