$(document).ready(function () {
    /**
     * #Инфинити
     */
    var loading = false;
    $(document).on('click', '#infinity-next-page', function (e) {
        var infinity_button = $(this);
        var infinity_button_container = $(this).parent().parent();

        if (!loading) {
            loading = true;
            infinity_button.addClass('loading').find('.infinity__link-text').text(infinity_button.data('loadText'));
            $.get(
                infinity_button.attr('href'),
                {is_ajax: 'y'},
                function (data) {
                    infinity_button_container.after(data);
                    infinity_button_container.remove();
                    loading = false;
                }
            );
        }
        e.preventDefault();
    });
});