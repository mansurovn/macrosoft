$(document).ready(function () {
    var statusSlider = $(".status__slider");
    if (statusSlider.length) {
        statusSlider.slick({
            autoplay: false,
            arrow: true,
            prevArrow: '<button class="status-slider__arrow status-slider__arrow_left icon icon_arrow-left"></button>',
            nextArrow: '<button class="status-slider__arrow status-slider__arrow_right icon icon_arrow-right"></button>',
            draggable: true,
            infinite: false,
            dots: true,
            accessibility: false
        })
    }

    if ($('[data-fancybox]').length) {
        $().fancybox({
            selector: '[data-fancybox="images"]',
            thumbs: false,
            hash: false,
            infobar: true,
            lang: 'ru',
            i18n: {
                'ru': {
                    CLOSE: 'Закрыть',
                    NEXT: 'Следующий',
                    PREV: 'Предыдущий',
                    ERROR: 'Не удалось загрузить контент',
                    PLAY_START: 'Старт',
                    PLAY_STOP: 'Пауза',
                    FULL_SCREEN: 'Полный экран',
                    THUMBS: 'Миниатюры'
                }
            }
        });
    }
})