$(document).ready(function () {
    var link = $('.attendance-item-cats__cat');

    if (link.length){
        link.on('click', function (event) {
            $('html, body').animate({
                scrollTop: $('#rates').offset().top
            });
            event.preventDefault();
        });
    }
});