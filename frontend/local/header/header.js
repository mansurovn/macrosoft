$(document).ready(function (e) {
    var location = $(".header__location");
    var locationList = $('.header-location__list');
    var locationItems = locationList.find('.header-location__item');
    var locationBtn = $('.header-location__btn');
    var locationLink = $('.header-location__link');
    var tel = $('.header__tel');

    if (locationList.length) {
        locationBtn.on('click', function () {
            location.toggleClass('header__location_active');
            locationList.fadeToggle('fast');

            location.on('mouseleave', function () {
                location.removeClass('header__location_active');
                locationList.fadeOut('fast');
            });
        });
        locationItems.on('click', function(){
            locationItems.removeClass('header-location__item_active');
            locationLink.text($(this).text());
            tel.text($(this).data('tel'));
            $(this).addClass('header-location__item_active');
            locationBtn.trigger('click');
        });
    }
});