$(document).ready(function(){
    var modalTitle = $('.products-vendors-modal__title');
    var modalText = $('.products-vendors-modal__text');
    var modalLink = $('.products-vendors-modal__btn-link');
    $('.products-vendors-row__item_modal').on('click', function(){
        var link = $(this).find('.products-vendors__link_none').text();
        modalTitle.empty().append($(this).find('.products-vendors__title_none').html());
        modalText.empty().append($(this).find('.products-vendors__text_none').html());
        if (link != "") {
            modalLink.parent().show();
            modalLink.attr('href', $(this).find('.products-vendors__link_none').text());
        } else {
            modalLink.parent().hide();
        }
    })
        .fancybox({
        padding: 0,
        caption: '',
        touch: false,
        baseClass: 'fancybox-custom',
        lang: 'ru',
        modal: true,
        i18n: {
            'ru': {
                CLOSE: 'Закрыть'
            }
        }
    });

    $('.products-vendors-modal__close').on('click', function(){
        fancybox.close();
    })
});