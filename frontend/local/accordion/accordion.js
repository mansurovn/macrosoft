$(document).ready(function () {
    var anchor = $('.accordion__anchor');
    var collapsedItem = $('[data-collapse-id]');
    var target = $('.accordion__target');
    var maxHeight = [];
    var minHeight = 26;

    setTimeout(function () {
        if (collapsedItem.length) {
            collapsedItem.each(function(index, value) {
                var target = $(this).find('.accordion__target');

                maxHeight[$(this).data('collapseId')] = target.height();
                target.height(minHeight);
            })
        }
    }, 50);


    if (anchor.length) {
        anchor.on('click', function (event) {
            var curItem = $(this).hasClass('accordion__item') ? $(this) : $(this).closest('.accordion__item');

            if (curItem.data('collapseId') !== undefined) {
                var collapsed = curItem.find('.accordion__target');

                if (collapsed.height() <= minHeight) {
                    collapsed.height(maxHeight[curItem.data('collapseId')]);
                } else {
                    collapsed.height(minHeight);
                }
                //collapsed.toggleClass('accordion__collapsed');
                curItem.toggleClass('accordion__item_expand');
            } else if (curItem.data('hide') !== undefined) {
                var hided = curItem.children('.accordion__target');

                //hided.toggleClass('accordion__hided');
                hided.slideToggle();
                curItem.toggleClass('accordion__item_expand');
            }

        });
    }
});