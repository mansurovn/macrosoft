$(document).ready(function () {
    var slider = $('.review__slider');
    var slide = $('.review__slide');
    var minHeight = 73;
    var maxHeight = [];
    var btn = $('.review__expander');
    var link = btn.children('.review__btn-link');

    slide.each(function (item, value) {
        var text = $(this).find('.review-slide__text');
        maxHeight[item] = text.height();
        text.height(minHeight);
        if (item == 0) {
            if (maxHeight[item] > minHeight) {
                btn.show();
            } else {
                btn.hide();
            }
        }
    });

    if (slider.length) {
        slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
            var curSlide = $('.review-slide-wrap[data-slick-index="'+ currentSlide +'"]');
            var nxtSlide = $('.review-slide-wrap[data-slick-index="'+ nextSlide +'"]');
            var text = curSlide.find('.review-slide__text');

            if (maxHeight[nextSlide] > minHeight) {
                btn.fadeIn(300);
            } else {
                btn.fadeOut(300);
            }
            if (text.height() > minHeight && text.height() <= maxHeight[currentSlide]) {
                text.height(minHeight);
                link.text('Читать отзыв целиком');
            }
        })
            .slick({
            autoplay: false,
            arrow: true,
            prevArrow: '<button class="review-slider__arrow review-slider__arrow_left icon icon_arrow-left"></button>',
            nextArrow: '<button class="review-slider__arrow review-slider__arrow_right icon icon_arrow-right"></button>',
            appendArrows: $('.review__body'),
            draggable: true,
            infinite: false,
            focusOnSelect: true,
            accessibility: false,
            adaptiveHeight: false,
        })
    }

    btn.on('click', function(){
        var curSlide = $('.review__slider .slick-current.slick-active');
        var i = curSlide.data('slick-index');
        var text = curSlide.find('.review-slide__text');

        if (text.height() < maxHeight[i]) {
            text.height(maxHeight[i]);
            link.text('Cкрыть');
        } else {
            text.height(minHeight);
            link.text('Читать отзыв целиком');
        }
    });
});
