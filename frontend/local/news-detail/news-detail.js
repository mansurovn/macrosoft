$(document).ready(function(){
    var slider = $('.news-detail__slider');
    if (slider.length) {
        slider.slick({
            arrow: true,
            prevArrow: '<button class="news-detail-slider__arrow news-detail-slider__arrow_up icon icon_arrow-up"></button>',
            nextArrow: '<button class="news-detail-slider__arrow news-detail-slider__arrow_down icon icon_arrow-down"></button>',
            dots: true,
            accessibility: false,
            autoplay: false,
            vertical: true,
            draggable: false
        })
    }

    var images = $('[data-fancybox="images"]');

    if (images.length) {
        images.fancybox({
            thumbs: false,
            hash: false,
            infobar: true,
            lang: 'ru',
            i18n: {
                'ru': {
                    CLOSE: 'Закрыть',
                    NEXT: 'Следующий',
                    PREV: 'Предыдущий',
                    ERROR: 'Не удалось загрузить контент',
                    PLAY_START: 'Старт',
                    PLAY_STOP: 'Пауза',
                    FULL_SCREEN: 'Полный экран',
                    THUMBS: 'Миниатюры'
                }
            }
        })
    }
});