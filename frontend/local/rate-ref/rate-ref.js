window.isTabOpen = false;

$(document).ready(function () {
    var refLinks = $('.rate-ref__link');
    var refAnchor = $('.rate-ref__anchor');
    var refHider = $('.rate-ref__hider');
    var refBlock = $('.rate-ref');



    if (refBlock.length) {
        refLinks.on('click', function (event, triggered) {
            var curLink = $(this);
            var tabpanelCollapsed = refAnchor.css('display') === 'none';

            if (!triggered) {
                if (curLink.hasClass('rate-ref__link_expand')) {
                    refLinks.removeClass('rate-ref__link_expand rate-panel-about__link_expand').text('Подробности').attr('title', 'Подробности');
                    refAnchor.slideUp(400);
                } else {
                    if (tabpanelCollapsed) {
                        curLink.addClass('rate-ref__link_expand rate-panel-about__link_expand').text('Скрыть').attr('title', 'Скрыть');
                        refAnchor.slideDown(400);
                        $('html, body').delay('400').animate({                                                                  //  Скролл до таблицы
                            scrollTop: refAnchor.offset().top
                        }, 400);
                    } else
                    {
                        refLinks.removeClass('rate-ref__link_expand rate-panel-about__link_expand').text('Подробности').attr('title', 'Подробности');
                        curLink.addClass('rate-ref__link_expand rate-panel-about__link_expand').text('Скрыть').attr('title', 'Скрыть');
                    }
                }
                $('.rate-tabs__tab[href="' + curLink.attr('href') + '"]').trigger('click', true);                           // Вызываем клик по табу
            } else {
                if (!curLink.hasClass('rate-ref__link_expand')) {
                    refLinks.removeClass('rate-ref__link_expand rate-panel-about__link_expand').text('Подробности').attr('title', 'Подробности');
                    curLink.addClass('rate-ref__link_expand rate-panel-about__link_expand').text('Скрыть').attr('title', 'Скрыть');
                }
            }

        });

        refHider.on('click', function (event) {                     // Свернуть все
            refLinks.removeClass('rate-ref__link_expand rate-panel-about__link_expand').text('Подробности').attr('title', 'Подробности');
            refAnchor.slideUp();
        });
    }

    function expandLink(link) {
        link.addClass('rate-ref__link_expand');                                                             // Смена стилей триггера на открытый
        link.addClass('rate-panel-about__link_expand').text('Скрыть').attr('title', 'Скрыть');
    }

    function collapseLink(){
        refLinks.removeClass('rate-panel-about__link_expand').text('Подробности').attr('title', 'Подробности');     // Смена стилей триггера на закрытый
        refLinks.removeClass('rate-ref__link_expand');
    }
});