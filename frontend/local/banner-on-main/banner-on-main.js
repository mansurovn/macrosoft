$(document).ready(function(){
    if ($('.banner-on-main__slider').length != 0) {
        $('.banner-on-main__slider').slick({
            accessibility: false,
            dots: true,
            arrows: true,
            prevArrow: '<div class="banner-on-main-slider__arrow banner-on-main-slider__arrow_left icon icon_arrow-left"></div>',
            nextArrow: '<div class="banner-on-main-slider__arrow banner-on-main-slider__arrow_right icon icon_arrow-right"></div>',
            autoplay: false
        });
    }
});