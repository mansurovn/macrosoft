$(document).ready(function(){
    var vacancy__wrap = $('.vacancies-wrap');
    var detail = $('.vacancies-vacancy__detail');
    var toggle = $('.vacancies__toggle-link');

    if (detail.length) {
        vacancy__wrap.on('click','.vacancies__toggle-link', function(){
            var cur_toggle = $(this);
            var cur_detail =cur_toggle.closest('.vacancies__vacancy').find('.vacancies-vacancy__detail');
            if (cur_detail.css('display') == 'none') {
               cur_toggle.text('Скрыть');
               cur_toggle.parent().addClass('vacancies__toggle_expand');
            } else {
               cur_toggle.text('Подробнее');
               cur_toggle.parent().removeClass('vacancies__toggle_expand');
            }
            cur_detail.slideToggle();
        });
    }
});