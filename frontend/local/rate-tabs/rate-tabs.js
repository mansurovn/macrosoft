$(document).ready(function(){
    var tablist = $('.rate-tabs__tablist');

    if (tablist.length) {
        var tabs = $('.rate-tabs__tab');

        tabs.on('click', function(event, triggered){
            var nextPanel = $($(this).attr('href'));
            var curPanel = $('.tab-panel_active.tab-panel_in');
            var tabPanels = nextPanel.closest('.rate-tabs__tabpanels');

            tabs.removeClass('rate-tabs__tab_active');              // переключение стилей вкладок
            $(this).addClass('rate-tabs__tab_active');

            if (!triggered){
                $('.rate-panel-about__link[href="' + $(this).attr('href') + '"]').trigger('click', true);
            }

            setTimeout(function(){
                tabPanels.height(nextPanel.outerHeight());         // Плавное изменение высоты панелей
            }, 300);

        });
    }
});