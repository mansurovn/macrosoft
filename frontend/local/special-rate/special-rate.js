$(document).ready(function(){
    var tabLink = $('.special-rate__tab');
    var toggle = $('.special-rate__toggle');
    var content = $('.special-rate__content');
    var tabPanels = $('.special-rate__tabpanels');
    var link = $('.special-rate-panel__btn-link');
    var form = $('#application');
    var page = form.find('[name="REMOTE_PAGE"]');
    var rate = form.find('[name="RATE"]');
    var period = form.find('[name="PERIOD"]');

    if (tabLink.length) {
        tabLink.on("click", function(event){
            var nextPanel = $($(this).attr('href'));

            tabLink.removeClass('special-rate__tab_active');
            $(this).addClass('special-rate__tab_active');

            setTimeout(function(){
                tabPanels.height(nextPanel.outerHeight());         // Плавное изменение высоты панелей
            }, 300);
        })
    }

    if (toggle.length) {
        toggle.on('click', function(event){
            content.slideToggle('400');
            if (!$(this).hasClass('special-rate__toggle_active')) {
                $('html, body').delay('400').animate({                                                                  //  Скролл до таблицы
                    scrollTop: content.offset().top
                }, 400);
                $(this).text('Скрыть специализированные тарифы');
            } else {
                $(this).text('Показать специализированные тарифы');
            }
            $(this).toggleClass('special-rate__toggle_active');
        })
    }

    link.on('click', function (event) {
        var curPanel = $(this).closest('.special-rate__panel');
        var curHeader = curPanel.find('.special-rate__duration').text();
        var curSubheader = curPanel.find('.special-rate__sale').text();
        var curTabPanelName = $('.special-rate__tab_active').text() + (curHeader!=="" ? (" / " + curHeader) : "") + (curSubheader!=="" ? (" - " + curSubheader) : "");

        page.val(window.location.href);
        rate.val(curTabPanelName);
    })

});