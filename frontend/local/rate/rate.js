$(document).ready(function () {
    // Списки в панелях
    var triggers = $('.rate-panel__variant');
    var lists = $('.pop-up__list');
    var formLink = $('.rate-panel__btn-link');
    var form = $('#application');
    var page = form.find('[name="REMOTE_PAGE"]');
    var rate = form.find('[name="RATE"]');
    var period = form.find('[name="PERIOD"]');

    if (lists.length) {
        triggers.on('click', function(event){
            var curList = $(this).find('.pop-up');
            var curPanel = curList.closest('.rate__panel');
            var curTrigger = $(this);
            //console.log(curList);
            //curList.fadeIn('fast');
            if (curList.hasClass('pop-up_active')) {
                //listHide(curList);
            } else {
                //listShow(curList);
            }
            curList.find('.pop-up-list__item').on('click', function(event){
                var priceField = curPanel.find('.rate-panel__price');
                var priceFieldAdd = curPanel.find('.rate-panel__price_add');

                curTrigger.find('.rate-panel-variant__current').text($(this).text());                   // текущий период

                // меняем цены в соответствие с периодом
                if ($(this).data('price')) {
                    priceField.eq(0).find('.rate-panel-price__number').text($(this).data('price'));
                    priceField.css('opacity', 1);
                } else {
                    priceField.css('opacity', 0);
                }

                if ($(this).data('priceAdd')) {
                    priceFieldAdd.eq(0).find('.rate-panel-price__number').text($(this).data('priceAdd'));
                    priceFieldAdd.css('opacity', 1);
                } else {
                    priceFieldAdd.css('opacity', 0);
                }

                curList.find('.pop-up-list__item').removeClass('pop-up-list__item_active');             // стиль для выбранного пункта
                $(this).addClass('pop-up-list__item_active');
                return false;
            });
        });
    }

    formLink.on('click', function(event){
        var curPanel = $(this).closest('.rate__panel');

        page.val(window.location.href);
        rate.val(curPanel.find('.rate-panel__header').text());
        period.val(curPanel.find('.rate-panel-variant__current').text());
    });
});