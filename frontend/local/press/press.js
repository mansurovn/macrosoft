$(document).ready(function () {
    var pressSlider = $('.press__slider');
    if (pressSlider.length) {
        pressSlider.slick({
            autoplay: false,
            arrow: true,
            prevArrow: '<button class="press-slider__arrow icon icon_arrow-left"></button>',
            nextArrow: '<button class="press-slider__arrow icon icon_arrow-right"></button>',
            appendArrows: $('.press-slider__toolbar'),
            slidesToShow: 4,
            slidesToScroll: 1,
            draggable: true,
            infinite: false,
            focusOnSelect: true,
            accessibility: false,
        });
        /*pressSlider.slick('slickGoTo', Math.floor(pressSlider.find('.press__slide:not(.slick-cloned)')/2));*/
    }
});
