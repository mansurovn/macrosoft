$(document).ready(function () {
    var item = $('.pop-up-list__item');
    var link = $('.attendance-detail-panel-about__link');
    var modal = $('.attendance-detail-panel__modal');

    if (item.length) {
        item.on('click', function(event){
            var panel = $(this).closest('.attendance-detail__panel');
            var price = $(this).data('price');
            var placer = panel.find('.attendance-detail-panel-price__value');

            if (price !== undefined) {
                placer.text(price);
            }
        })
    }

    if (modal.length) {
        $().fancybox({
            selector: '.attendance-detail-panel-about__link',
            padding: 0,
            caption: '',
            touch: false,
            baseClass: 'fancybox-custom',
            lang: 'ru',
            i18n: {
                'ru': {
                    CLOSE: 'Закрыть'
                }
            },
            btnTpl: {
                smallBtn: '<button data-fancybox-close class="icon icon_close attendance-detail-panel-modal__close" title="Закрыть"></button>'
            },
        });
    }
});