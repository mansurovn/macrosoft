$(document).ready(function () {
    var sidebarTogglers = $('.sidebar-multilevel__icon');

    if (sidebarTogglers.length) {
        sidebarTogglers.on('click', function(){
            $(this).toggleClass('sidebar-multilevel__icon_active');
            $(this).siblings('.sidebar-multilevel__submenu').slideToggle('fast');
        })
    }
});